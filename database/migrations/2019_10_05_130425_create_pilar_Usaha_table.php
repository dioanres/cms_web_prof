<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePilarUsahaTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pilar_Usaha', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nama_pilar_usaha')->nullable();
            $table->string('judul_deskripsi')->nullable();
            $table->text('deskripsi')->nullable();
            $table->text('value_pilar_usaha')->nullable();
            $table->string('logo')->nullable();
            $table->text('alamat')->nullable();
            $table->string('no_telp')->nullable();
            $table->string('no_hp')->nullable();
            $table->string('keterangan')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('pilar_Usaha');
    }
}
