<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCatalogPilarUsahasTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('CatalogPilarUsahas', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_pilar_usaha')->nullable();
            $table->text('image')->nullable();
            $table->float('harga')->nullable();
            $table->text('deskripsi')->nullable();
            $table->string('jenis_produk')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('CatalogPilarUsahas');
    }
}
