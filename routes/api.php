<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(['prefix' => 'data', 'namespace' => '\Api'], function (){
    Route::get('/','ApiController@getAllData')->name('api.data');
});

Route::group(['prefix' => 'career', 'namespace' => '\Api'], function (){
    Route::post('/register','ApiController@registerEmployee')->name('api.career.register');
});

Route::group(['prefix' => 'home','namespace' => '\Api'], function () {
    Route::get('/getImageSlider/{type}', 'HomeController@getImageSlider')->name('home.getImageSlider');
    Route::post('/formOnline','ApiController@createFromOnline')->name('api.home.formOnline');
    Route::get('/getImageSliderByBussinessUnit/{id}','ApiController@getImageSliderByBussinessUnit')->name('api.bussiness.unit');
});


Route::group(['prefix' => 'blog','namespace' => '\Api'], function () {
    Route::get('/', 'ApiController@getAllBlog')->name('api.getAllBlog');
});

Route::group(['prefix' => 'social-media','namespace' => '\Api'], function () {
    Route::get('/', 'ApiController@getSocialMedia')->name('api.social-media');
});


Route::group(['prefix' => 'content','namespace' => '\Api'], function () {
    Route::get('/about-us', 'ApiController@getAboutUs')->name('api.content.about-us');
    Route::get('/pilar-usaha', 'ApiController@getBussinessUnit')->name('api.content.pilar-usaha');
    Route::get('{id}/pilar-usaha', 'ApiController@getBussinessUnitById')->name('api.content.pilar-usaha');
    Route::get('clients', 'ApiController@getClients')->name('api.content.clients');
    Route::get('alamat-pab', 'ApiController@getAlamatPabGroup')->name('api.content.alamat-pab');
    Route::get('karyawan', 'ApiController@getKaryawan')->name('api.content.karyawan');
    Route::get('value-pab', 'ApiController@getValueCompany')->name('api.content.value-pab');
    Route::get('detail-misi', 'ApiController@getDetailMisi')->name('api.content.detail-misi');
    Route::get('career', 'ApiController@getCareer')->name('api.content.career');
    Route::get('{id}/detail-pilar-usaha', 'ApiController@getDetailPilarUsaha')->name('api.content.detail-pilar-usaha');
    Route::get('setting', 'ApiController@getSetting')->name('api.content.setting');
    Route::get('service', 'ApiController@getService')->name('api.content.service');
});
