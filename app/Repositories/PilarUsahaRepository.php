<?php

namespace App\Repositories;

use App\Models\PilarUsaha;
use InfyOm\Generator\Common\BaseRepository;

class PilarUsahaRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'nama_pilar_usaha',
        'judul_deskripsi',
        'deskripsi',
        'value_pilar_usaha',
        'logo',
        'alamat',
        'no_telp',
        'no_hp',
        'keterangan'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return PilarUsaha::class;
    }

    public function selectPilarUsaha()
    {
        return PilarUsaha::select('id','nama_pilar_usaha')->pluck('nama_pilar_usaha','id');
    }
}
