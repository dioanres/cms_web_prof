<?php

namespace App\Repositories;

use App\Models\MenuUtama;
use InfyOm\Generator\Common\BaseRepository;

class MenuUtamaRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'nama_menu'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return MenuUtama::class;
    }
}
