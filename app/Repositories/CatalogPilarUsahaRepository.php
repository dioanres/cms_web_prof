<?php

namespace App\Repositories;

use App\Models\CatalogPilarUsaha;
use InfyOm\Generator\Common\BaseRepository;

class CatalogPilarUsahaRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'id_pilar_usaha',
        'image',
        'harga',
        'deskripsi',
        'jenis_produk'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return CatalogPilarUsaha::class;
    }
}
