<?php

namespace App\Repositories\Home;

use App\Models\Home\ImageSlider;
use InfyOm\Generator\Common\BaseRepository;

class ImageSliderRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'image',
        'Path_image'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return ImageSlider::class;
    }

    public function getAll($type)
    {
        return ImageSlider::where('type',$type)->get();
    }

    public function getByBussinessUnit()
    {
        return ImageSlider::with('pilar_usaha')->where('id_pilar_usaha','!=',null)->get();
    }
}
