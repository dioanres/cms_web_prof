<?php

namespace App\Repositories;

use App\Models\Visimisi;
use InfyOm\Generator\Common\BaseRepository;

class VisimisiRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'id_pilar_usaha',
        'visi',
        'misi'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Visimisi::class;
    }

    public function checkExistPilarUsaha($idPilarUsaha)
    {
        return Visimisi::where('id_pilar_usaha',$idPilarUsaha)->count();
    }
}

