<?php

namespace App\Repositories;

use App\Models\ValueCompany;
use InfyOm\Generator\Common\BaseRepository;

class ValueCompanyRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'value',
        'deskripsi_value'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return ValueCompany::class;
    }
}
