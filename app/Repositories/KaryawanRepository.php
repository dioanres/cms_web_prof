<?php

namespace App\Repositories;

use App\Models\Karyawan;
use InfyOm\Generator\Common\BaseRepository;

class KaryawanRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'nama',
        'jabatan',
        'catatan',
        'foto',
        'ig',
        'twitter',
        'fb'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Karyawan::class;
    }
}
