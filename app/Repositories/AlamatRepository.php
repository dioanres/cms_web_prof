<?php

namespace App\Repositories;

use App\Models\Alamat;
use InfyOm\Generator\Common\BaseRepository;

class AlamatRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'alamat'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Alamat::class;
    }
}
