<?php

namespace App\Repositories;

use App\Models\JenisProdukKatalog;
use InfyOm\Generator\Common\BaseRepository;

class JenisProdukKatalogRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'nama_jenis_produk'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return JenisProdukKatalog::class;
    }

    public function select()
    {
        return JenisProdukKatalog::select('id','nama_jenis_produk')->pluck('nama_jenis_produk','id');
    }
}
