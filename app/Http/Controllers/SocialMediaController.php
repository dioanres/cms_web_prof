<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\SocialMedia;
use Flash;
use Illuminate\Support\Facades\Lang;

class SocialMediaController extends Controller
{
    public function index(Request $request)
    {
        $social_media = SocialMedia::all();
        
        return view('admin.social_media.index')
            ->with('social_media', $social_media);
    }

    public function create()
    {
        $social_media = null;
        return view('admin.social_media.create')->with(['social_media' => $social_media]);
    }

    public function store(Request $request)
    {
        $input = $request->all();
        
        if ($request->hasFile('icon')){
            $file = $request->file('icon');
            $extension = $file->extension()?: 'png';
            $picture = str_random(10) . '.' . $extension;
            $destinationPath = '/uploads/social_media/';
            $file->move($destinationPath, $picture);
            $input['icon'] = $picture;
            $input['path'] = $destinationPath;
        }
        //dd($input);
        SocialMedia::create($input);
        //$jenisProdukKatalog = $this->jenisProdukKatalogRepository->create($input);

        Flash::success('save success.');

        return redirect(route('admin.social_media.index'));
    }

    public function edit($id)
    {
        $social_media = SocialMedia::find($id);
    
        if (empty($social_media)) {
            Flash::error('Social Media not found');

            return redirect(route('social_media.index'));
        }

        return view('admin.social_media.edit')->with(['social_media' => $social_media]);
    }

    public function update($id, Request $request)
    {
        $social_media = SocialMedia::find($id);

        $input = $request->all();

        if (empty($social_media)) {
            Flash::error('Social Media not found');

            return redirect(route('social_media.index'));
        }

        if ($request->hasFile('icon')){
            $file = $request->file('icon');
            $extension = $file->extension()?: 'png';
            $picture = str_random(10) . '.' . $extension;
            $destinationPath = '/uploads/social_media/';
            $file->move($destinationPath, $picture);
            $input['icon'] = $picture;
            $input['path'] = '/uploads/social_media/';
        }

        $social_media->icon = $input['icon'];
        $social_media->name = $input['name'];
        $social_media->url = $input['url'];
        $social_media->updated_at = date('Y-m-d');
        $social_media->save();

        //$career = $this->careerRepository->update($input, $id);

        Flash::success('Social Media success updated.');

        return redirect(route('admin.social_media.index'));
    }

    public function getModalDelete($id = null)
      {
          $error = '';
          $model = '';
          $confirm_route =  route('admin.social_media.delete',['id'=>$id]);
          return View('admin.layouts/modal_confirmation', compact('error','model', 'confirm_route'));

      }

       public function getDelete($id = null)
       {
           $sample = SocialMedia::destroy($id);

           // Redirect to the group management page
           return redirect(route('admin.social_media.index'))->with('success', Lang::get('message.success.delete'));

       }
}
