<?php
namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Requests\CreateCatalogPilarUsahaRequest;
use App\Http\Requests\UpdateCatalogPilarUsahaRequest;
use App\Repositories\CatalogPilarUsahaRepository;
use App\Http\Controllers\AppBaseController as InfyOmBaseController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Lang;
use App\Models\CatalogPilarUsaha;
use App\Repositories\JenisProdukKatalogRepository;
use App\Repositories\PilarUsahaRepository;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class CatalogPilarUsahaController extends InfyOmBaseController
{
    /** @var  CatalogPilarUsahaRepository */
    private $catalogPilarUsahaRepository;
    private $pilarUsahaRepository;
    private $jenisProdukKatalogRepository;

    public function __construct(CatalogPilarUsahaRepository $catalogPilarUsahaRepo, 
                                PilarUsahaRepository $pilarUsahaRepository,
                                JenisProdukKatalogRepository $jenisProdukKatalogRepository)
    {
        $this->catalogPilarUsahaRepository = $catalogPilarUsahaRepo;
        $this->pilarUsahaRepository =  $pilarUsahaRepository;
        $this->jenisProdukKatalogRepository = $jenisProdukKatalogRepository;
    }

    /**
     * Display a listing of the CatalogPilarUsaha.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->catalogPilarUsahaRepository->pushCriteria(new RequestCriteria($request));
        $catalogPilarUsahas = $this->catalogPilarUsahaRepository->all();

        return view('admin.catalogPilarUsahas.index')
            ->with('catalogPilarUsahas', $catalogPilarUsahas);
    }

    /**
     * Show the form for creating a new CatalogPilarUsaha.
     *
     * @return Response
     */
    public function create()
    {
        $pilarUsahas = $this->pilarUsahaRepository->selectPilarUsaha();
        $jenisProduk = $this->jenisProdukKatalogRepository->select();
        $catalogPilarUsaha = null;
        return view('admin.catalogPilarUsahas.create',compact(['pilarUsahas',
                                                                'catalogPilarUsaha',
                                                                'jenisProduk']));
    }

    /**
     * Store a newly created CatalogPilarUsaha in storage.
     *
     * @param CreateCatalogPilarUsahaRequest $request
     *
     * @return Response
     */
    public function store(CreateCatalogPilarUsahaRequest $request)
    {
        $input = $request->all();
        $input['harga'] = (float)$input['harga'];
        if ($request->hasFile('image')){
            $file = $request->file('image');
            $extension = $file->extension()?: 'png';
            $picture = str_random(10) . '.' . $extension;
            $destinationPath = public_path() . '/uploads/catalog/';
            $file->move($destinationPath, $picture);
            $input['image'] = $picture;
        }
        if($input['harga']) {
            $input['harga'] = (integer)str_replace(',','', $input['harga']);
        }
        $catalogPilarUsaha = $this->catalogPilarUsahaRepository->create($input);

        Flash::success('CatalogPilarUsaha saved successfully.');

        return redirect(route('admin.catalogPilarUsahas.index'));
    }

    /**
     * Display the specified CatalogPilarUsaha.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $catalogPilarUsaha = $this->catalogPilarUsahaRepository->findWithoutFail($id);

        if (empty($catalogPilarUsaha)) {
            Flash::error('CatalogPilarUsaha not found');

            return redirect(route('catalogPilarUsahas.index'));
        }

        return view('admin.catalogPilarUsahas.show')->with('catalogPilarUsaha', $catalogPilarUsaha);
    }

    /**
     * Show the form for editing the specified CatalogPilarUsaha.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $catalogPilarUsaha = $this->catalogPilarUsahaRepository->findWithoutFail($id);
        $pilarUsahas = $this->pilarUsahaRepository->selectPilarUsaha();
        $jenisProduk = $this->jenisProdukKatalogRepository->select();

        if (empty($catalogPilarUsaha)) {
            Flash::error('CatalogPilarUsaha not found');

            return redirect(route('catalogPilarUsahas.index'));
        }

        return view('admin.catalogPilarUsahas.edit')->with([
                                                            'catalogPilarUsaha' => $catalogPilarUsaha,
                                                            'pilarUsahas' => $pilarUsahas,
                                                            'jenisProduk' => $jenisProduk
                                                            ]);
    }

    /**
     * Update the specified CatalogPilarUsaha in storage.
     *
     * @param  int              $id
     * @param UpdateCatalogPilarUsahaRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateCatalogPilarUsahaRequest $request)
    {
        $catalogPilarUsaha = $this->catalogPilarUsahaRepository->findWithoutFail($id);

        if (empty($catalogPilarUsaha)) {
            Flash::error('CatalogPilarUsaha not found');

            return redirect(route('catalogPilarUsahas.index'));
        }
        $input = $request->all();
        if ($request->hasFile('image')){
            $file = $request->file('image');
            $extension = $file->extension()?: 'png';
            $picture = str_random(10) . '.' . $extension;
            $destinationPath = public_path() . '/uploads/catalog/';
            $file->move($destinationPath, $picture);
            $input['image'] = $picture;
        }
        if($input['harga']) {
            $input['harga'] = (integer)str_replace(',','', $input['harga']);
        }
        $catalogPilarUsaha = $this->catalogPilarUsahaRepository->update($input, $id);

        Flash::success('CatalogPilarUsaha updated successfully.');

        return redirect(route('admin.catalogPilarUsahas.index'));
    }

    /**
     * Remove the specified CatalogPilarUsaha from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
      public function getModalDelete($id = null)
      {
          $error = '';
          $model = '';
          $confirm_route =  route('admin.catalogPilarUsahas.delete',['id'=>$id]);
          return View('admin.layouts/modal_confirmation', compact('error','model', 'confirm_route'));

      }

       public function getDelete($id = null)
       {
           $sample = CatalogPilarUsaha::destroy($id);

           // Redirect to the group management page
           return redirect(route('admin.catalogPilarUsahas.index'))->with('success', Lang::get('message.success.delete'));

       }
}
