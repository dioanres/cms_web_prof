<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Blog;
use App\DetailMisi;
use App\Models\Content;
use App\Models\PilarUsaha;
use App\Models\Alamat;
use App\Models\Karyawan;
use App\Models\ValueCompany;
use App\Models\Visimisi;
use App\Models\Career;
use App\Models\Setting;
use App\Models\Service;
use App\Models\RegisterCareer;
use App\Models\FormOnline;
use App\Models\SocialMedia;
use App\Models\Home\ImageSlider;
use function PHPSTORM_META\type;

class ApiController extends Controller
{
    public function getAllBlog()
    {

        $all_data = Blog::all();
        $new_data = [];

        foreach ($all_data as $key => $value) {
            array_push($new_data,
                        ['id' => $value->id,
                         'blog_category_id' => $value->image,
                         'user_id' => $value->title,
                         'slug' => $value->created_at,
                         "content" => $value->content,
                         "image" => $value->image,
                         'path' => 'uploads/blog/',
                         "views" => $value->views,
                         "created_at" => $value->created_at,
                         "updated_at" => $value->updated_at,
                         "deleted_at" => $value->deleted_at
                        ]
                        );
        }
        $collect_data = ['status' => true, 'data' => $new_data];
        return $collect_data;
    }

    public function getAboutUs()
    {

        $data = Content::where('menu_utama',config('menu.about_us'))->get();
        return $data;
    }

    public function getDetailMisi()
    {
        $data = DetailMisi::all();
        return $data;
    }

    public function getValueCompany() {
        $value = ValueCompany::orderBy('id','desc')->get(); 
        return $value;
    }

    public function getBussinessUnit()
    {
        $data = PilarUsaha::all();
        
        return $data;
    }

    public function getBussinessUnitById($id){
        $data = $data =PilarUsaha::where('id',$id)->get();
        return $data;
    }

    public function getClients() {
        $data = Content::where('menu_utama',config('menu.clients'))->get();
        return $data;
    }

    public function getAlamatPabGroup() {
        $data = Alamat::first();
        return $data;
    }

    public function getCareer(){
        $data = Career::where('status',1)->get();
        return $data;
    }

    public function getKaryawan() {
        $data = Karyawan::all();
        return $data;
    }

    public function getSetting() {
        $data = Setting::first();
        return $data;
    }

    public function getService() {
        $data = Service::all();
        return $data;
    }

    public function getSocialMedia() {
        $data = SocialMedia::all();
        return $data;
    }

    public function getAllData(){
        $data = array(
                'about_us' => $this->getAboutUs(),
                'bussiness_unit' => $this->getBussinessUnit(),
                'social_media' => $this->getSocialMedia(),
                'services' => $this->getService(),
                'setting' => $this->getSetting(),
                'career' => $this->getCareer()
        );
        return $data;
    }

    public function getImageSliderByBussinessUnit($id){
        $images = ImageSlider::where('id_pilar_usaha',$id)->get();
        $new_data = [];

        foreach ($images as $key => $value) {
            array_push($new_data,
                        ['id' => $value->id,
                         'image' => $value->image,
                         'path' => 'uploads/home/slider/',
                         'id_pilar_usaha' => $value->id_pilar_usaha,
                         'title' => $value->title,
                         'sub_title' => $value->sub_title,
                         'description' => $value->description,
                         'created_at' => $value->created_at,
                         'updated_at' => $value->updated_at
                        ]
                        );
        }
        $collect_data = ['status' => true, 'data' => $new_data];
        return $new_data;
        //return $images;
    }

    public function getDetailPilarUsaha($id) {
        $pilarusaha =PilarUsaha::with('visi_misi','catalog.jenis_produk')->where('id',$id)->first();
        $data = array(
            'info_pilar_usaha' => $pilarusaha
        );
        return $data;
    }

    public function registerEmployee(Request $request) 
    {
        $input = $request->all();
        $input['cv'] = json_encode($input['cv']);
        try {
            RegisterCareer::create($input);
            return 'sukses';
        } catch(Exception $e) {
            return $e;
        }
    }

    public function createFromOnline(Request $request)
    {
        $input = $request->all();

        try {
            FormOnline::create($input);
            return 'sukses';
        } catch(Exception $e) {
            return $e;
        }

    }
}
