<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Home\ImageSlider;

class HomeController extends Controller
{
    public function getImageSlider($type)
    {
        $all_data = ImageSlider::where('type',$type)->get();
        $new_data = [];

        foreach ($all_data as $key => $value) {
            array_push($new_data,
                        ['id' => $value->id,
                         'image' => $value->image,
                         'path' => 'uploads/home/slider/',
                         'title' => $value->title,
                         'sub_title' => $value->sub_title,
                         'description' => $value->description,
                         'created_at' => $value->created_at,
                         'updated_at' => $value->updated_at
                        ]
                        );
        }
        $collect_data = ['status' => true, 'data' => $new_data];
        return $collect_data;
    }

}