<?php
namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Requests\CreateMenuUtamaRequest;
use App\Http\Requests\UpdateMenuUtamaRequest;
use App\Repositories\MenuUtamaRepository;
use App\Http\Controllers\AppBaseController as InfyOmBaseController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Lang;
use App\Models\MenuUtama;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class MenuUtamaController extends InfyOmBaseController
{
    /** @var  MenuUtamaRepository */
    private $menuUtamaRepository;

    public function __construct(MenuUtamaRepository $menuUtamaRepo)
    {
        $this->menuUtamaRepository = $menuUtamaRepo;
    }

    /**
     * Display a listing of the MenuUtama.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->menuUtamaRepository->pushCriteria(new RequestCriteria($request));
        $menuUtamas = $this->menuUtamaRepository->all();

        return view('admin.menuUtamas.index')
            ->with('menuUtamas', $menuUtamas);
    }

    /**
     * Show the form for creating a new MenuUtama.
     *
     * @return Response
     */
    public function create()
    {
        return view('admin.menuUtamas.create');
    }

    /**
     * Store a newly created MenuUtama in storage.
     *
     * @param CreateMenuUtamaRequest $request
     *
     * @return Response
     */
    public function store(CreateMenuUtamaRequest $request)
    {
        $input = $request->all();

        $menuUtama = $this->menuUtamaRepository->create($input);

        Flash::success('MenuUtama saved successfully.');

        return redirect(route('admin.menuUtamas.index'));
    }

    /**
     * Display the specified MenuUtama.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $menuUtama = $this->menuUtamaRepository->findWithoutFail($id);

        if (empty($menuUtama)) {
            Flash::error('MenuUtama not found');

            return redirect(route('menuUtamas.index'));
        }

        return view('admin.menuUtamas.show')->with('menuUtama', $menuUtama);
    }

    /**
     * Show the form for editing the specified MenuUtama.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $menuUtama = $this->menuUtamaRepository->findWithoutFail($id);

        if (empty($menuUtama)) {
            Flash::error('MenuUtama not found');

            return redirect(route('menuUtamas.index'));
        }

        return view('admin.menuUtamas.edit')->with('menuUtama', $menuUtama);
    }

    /**
     * Update the specified MenuUtama in storage.
     *
     * @param  int              $id
     * @param UpdateMenuUtamaRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateMenuUtamaRequest $request)
    {
        $menuUtama = $this->menuUtamaRepository->findWithoutFail($id);

        

        if (empty($menuUtama)) {
            Flash::error('MenuUtama not found');

            return redirect(route('menuUtamas.index'));
        }

        $menuUtama = $this->menuUtamaRepository->update($request->all(), $id);

        Flash::success('MenuUtama updated successfully.');

        return redirect(route('admin.menuUtamas.index'));
    }

    /**
     * Remove the specified MenuUtama from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
      public function getModalDelete($id = null)
      {
          $error = '';
          $model = '';
          $confirm_route =  route('admin.menuUtamas.delete',['id'=>$id]);
          return View('admin.layouts/modal_confirmation', compact('error','model', 'confirm_route'));

      }

       public function getDelete($id = null)
       {
           $sample = MenuUtama::destroy($id);

           // Redirect to the group management page
           return redirect(route('admin.menuUtamas.index'))->with('success', Lang::get('message.success.delete'));

       }
}
