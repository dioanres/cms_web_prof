<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Career;
use Flash;
use Illuminate\Support\Facades\Lang;

class CareerController extends Controller
{
    public function index(Request $request)
    {
        $careers = Career::all();
        
        return view('admin.career.index')
            ->with('careers', $careers);
    }

    public function create()
    {
        $career = null;
        $status = [1 => 'Aktif', 0 => 'Tidak Aktif'];
        return view('admin.career.create')->with(['career' => $career, 'status' => $status]);
    }

    public function store(Request $request)
    {
        $input = $request->all();
        $input['publish_date'] = date('Y-m-d', strtotime($input['publish_date']));

        //dd($input);
        Career::create($input);
        //$jenisProdukKatalog = $this->jenisProdukKatalogRepository->create($input);

        Flash::success('save success.');

        return redirect(route('admin.careers.index'));
    }

    public function edit($id)
    {
        $career = Career::find($id);
        $status = [1 => 'Aktif', 0 => 'Tidak Aktif'];
        if (empty($career)) {
            Flash::error('Career not found');

            return redirect(route('careers.index'));
        }

        return view('admin.career.edit')->with(['career' => $career, 'status' => $status]);
    }

    public function update($id, Request $request)
    {
        $career = Career::find($id);

        $input = $request->all();

        if (empty($career)) {
            Flash::error('Career not found');

            return redirect(route('careers.index'));
        }

        $career->job_title = $input['job_title'];
        $career->company = $input['company'];
        $career->location = $input['location'];
        $career->publish_date = date('Y-m-d', strtotime($input['publish_date']));
        $career->status = $input['status'];
        $career->updated_at = date('Y-m-d');
        $career->save();

        //$career = $this->careerRepository->update($input, $id);

        Flash::success('Career berhasil di update.');

        return redirect(route('admin.careers.index'));
    }

    public function getModalDelete($id = null)
      {
          $error = '';
          $model = '';
          $confirm_route =  route('admin.careers.delete',['id'=>$id]);
          return View('admin.layouts/modal_confirmation', compact('error','model', 'confirm_route'));

      }

       public function getDelete($id = null)
       {
           $sample = Career::destroy($id);

           // Redirect to the group management page
           return redirect(route('admin.careers.index'))->with('success', Lang::get('message.success.delete'));

       }
}
