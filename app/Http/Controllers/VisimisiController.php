<?php
namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Requests\CreateVisimisiRequest;
use App\Http\Requests\UpdateVisimisiRequest;
use App\Repositories\VisimisiRepository;
use App\Http\Controllers\AppBaseController as InfyOmBaseController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Lang;
use App\Models\Visimisi;
use App\Repositories\PilarUsahaRepository;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class VisimisiController extends InfyOmBaseController
{
    /** @var  VisimisiRepository */
    private $visimisiRepository;
    private $pilarUsahaRepository;

    public function __construct(VisimisiRepository $visimisiRepo, PilarUsahaRepository $pilarUsahaRepo)
    {
        $this->visimisiRepository = $visimisiRepo;
        $this->pilarUsahaRepository = $pilarUsahaRepo;
    }

    /**
     * Display a listing of the Visimisi.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->visimisiRepository->pushCriteria(new RequestCriteria($request));
        $visimisis = $this->visimisiRepository->all();

        return view('admin.visimisis.index')
            ->with('visimisis', $visimisis);
    }

    /**
     * Show the form for creating a new Visimisi.
     *
     * @return Response
     */
    public function create()
    {
        $pilarUsahas = $this->pilarUsahaRepository->selectPilarUsaha();
        $visimisi = null;
        return view('admin.visimisis.create')->with(['visimisi' => $visimisi,'pilar_usahas' => $pilarUsahas]);
    }

    /**
     * Store a newly created Visimisi in storage.
     *
     * @param CreateVisimisiRequest $request
     *
     * @return Response
     */
    public function store(CreateVisimisiRequest $request)
    {
        $input = $request->all();
        
        $cekpilarusaha = $this->visimisiRepository->checkExistPilarUsaha($input['id_pilar_usaha']);
        
        if($cekpilarusaha > 0) {
            $nama_pilar_usaha = $this->pilarUsahaRepository->findWithoutFail($input['id_pilar_usaha'])->nama_pilar_usaha;
            
            Flash::error('Visi Misi untuk pilar usaha '.$nama_pilar_usaha.' sudah ada.');
            //return redirect(route('admin.visimisis.create'));    
        } else {
            $visimisi = $this->visimisiRepository->create($input);

            Flash::success('Visimisi saved successfully.');

            
        }
        return redirect(route('admin.visimisis.index'));
    }

    /**
     * Display the specified Visimisi.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $visimisi = $this->visimisiRepository->findWithoutFail($id);

        if (empty($visimisi)) {
            Flash::error('Visimisi not found');

            return redirect(route('visimisis.index'));
        }

        return view('admin.visimisis.show')->with('visimisi', $visimisi);
    }

    /**
     * Show the form for editing the specified Visimisi.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $visimisi = $this->visimisiRepository->findWithoutFail($id);
        $pilarUsahas = $this->pilarUsahaRepository->selectPilarUsaha();
        if (empty($visimisi)) {
            Flash::error('Visimisi not found');

            return redirect(route('visimisis.index'));
        }

        return view('admin.visimisis.edit')->with(['visimisi' => $visimisi,'pilar_usahas' => $pilarUsahas]);
    }

    /**
     * Update the specified Visimisi in storage.
     *
     * @param  int              $id
     * @param UpdateVisimisiRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateVisimisiRequest $request)
    {
        $visimisi = $this->visimisiRepository->findWithoutFail($id);

        

        if (empty($visimisi)) {
            Flash::error('Visimisi not found');

            return redirect(route('visimisis.index'));
        }

        $visimisi = $this->visimisiRepository->update($request->all(), $id);

        Flash::success('Visimisi updated successfully.');

        return redirect(route('admin.visimisis.index'));
    }

    /**
     * Remove the specified Visimisi from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
      public function getModalDelete($id = null)
      {
          $error = '';
          $model = '';
          $confirm_route =  route('admin.visimisis.delete',['id'=>$id]);
          return View('admin.layouts/modal_confirmation', compact('error','model', 'confirm_route'));

      }

       public function getDelete($id = null)
       {
           $sample = Visimisi::destroy($id);

           // Redirect to the group management page
           return redirect(route('admin.visimisis.index'))->with('success', Lang::get('message.success.delete'));

       }
}
