<?php
namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Requests\CreateValueCompanyRequest;
use App\Http\Requests\UpdateValueCompanyRequest;
use App\Repositories\ValueCompanyRepository;
use App\Http\Controllers\AppBaseController as InfyOmBaseController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Lang;
use App\Models\ValueCompany;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class ValueCompanyController extends InfyOmBaseController
{
    /** @var  ValueCompanyRepository */
    private $valueCompanyRepository;

    public function __construct(ValueCompanyRepository $valueCompanyRepo)
    {
        $this->valueCompanyRepository = $valueCompanyRepo;
    }

    /**
     * Display a listing of the ValueCompany.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->valueCompanyRepository->pushCriteria(new RequestCriteria($request));
        $valueCompanies = $this->valueCompanyRepository->all();

        return view('admin.valueCompanies.index')
            ->with('valueCompanies', $valueCompanies);
    }

    /**
     * Show the form for creating a new ValueCompany.
     *
     * @return Response
     */
    public function create()
    {
        return view('admin.valueCompanies.create');
    }

    /**
     * Store a newly created ValueCompany in storage.
     *
     * @param CreateValueCompanyRequest $request
     *
     * @return Response
     */
    public function store(CreateValueCompanyRequest $request)
    {
        $input = $request->all();

        $count = ValueCompany::get()->count();
        
        if($count < 3 ) {
            $valueCompany = $this->valueCompanyRepository->create($input);
            Flash::success('ValueCompany saved successfully.');
        } else {
            Flash::error('Value Perusahaan Sudah lengkap 3.');
        }

        return redirect(route('admin.valueCompanies.index'));
    }

    /**
     * Display the specified ValueCompany.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $valueCompany = $this->valueCompanyRepository->findWithoutFail($id);

        if (empty($valueCompany)) {
            Flash::error('ValueCompany not found');

            return redirect(route('valueCompanies.index'));
        }

        return view('admin.valueCompanies.show')->with('valueCompany', $valueCompany);
    }

    /**
     * Show the form for editing the specified ValueCompany.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $valueCompany = $this->valueCompanyRepository->findWithoutFail($id);

        if (empty($valueCompany)) {
            Flash::error('ValueCompany not found');

            return redirect(route('valueCompanies.index'));
        }

        return view('admin.valueCompanies.edit')->with('valueCompany', $valueCompany);
    }

    /**
     * Update the specified ValueCompany in storage.
     *
     * @param  int              $id
     * @param UpdateValueCompanyRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateValueCompanyRequest $request)
    {
        $valueCompany = $this->valueCompanyRepository->findWithoutFail($id);

        

        if (empty($valueCompany)) {
            Flash::error('ValueCompany not found');

            return redirect(route('valueCompanies.index'));
        }

        $valueCompany = $this->valueCompanyRepository->update($request->all(), $id);

        Flash::success('ValueCompany updated successfully.');

        return redirect(route('admin.valueCompanies.index'));
    }

    /**
     * Remove the specified ValueCompany from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
      public function getModalDelete($id = null)
      {
          $error = '';
          $model = '';
          $confirm_route =  route('admin.valueCompanies.delete',['id'=>$id]);
          return View('admin.layouts/modal_confirmation', compact('error','model', 'confirm_route'));

      }

       public function getDelete($id = null)
       {
           $sample = ValueCompany::destroy($id);

           // Redirect to the group management page
           return redirect(route('admin.valueCompanies.index'))->with('success', Lang::get('message.success.delete'));

       }
}
