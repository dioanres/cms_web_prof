<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\FormOnline;
use Flash;
use Illuminate\Support\Facades\Lang;

class FormOnlineController extends Controller
{
    public function index(Request $request)
    {
        $form_onlines = FormOnline::all();
        $title = "Form Online";
        
        return view('admin.form_online.index')
            ->with(['form_onlines' => $form_onlines, 'title' => $title]);
    }

    public function create()
    {
        $register_career = null;
        $status = [1 => 'Aktif', 0 => 'Tidak Aktif'];
        return view('admin.register_career.create')->with(['register_career' => $register_career, 'status' => $status]);
    }

    public function store(Request $request)
    {
        $input = $request->all();
        $input['publish_date'] = date('Y-m-d', strtotime($input['publish_date']));

        Career::create($input);

        Flash::success('save success.');

        return redirect(route('admin.register_career.index'));
    }

    public function edit($id)
    {
        $career = Career::find($id);
        $status = [1 => 'Aktif', 0 => 'Tidak Aktif'];
        if (empty($career)) {
            Flash::error('Career not found');

            return redirect(route('registers_career.index'));
        }

        return view('admin.register_career.edit')->with(['register_career' => $career, 'status' => $status]);
    }

    public function update($id, Request $request)
    {
        $career = Career::find($id);

        $input = $request->all();

        if (empty($career)) {
            Flash::error('Career not found');

            return redirect(route('register_careers.index'));
        }

        $career->job_title = $input['job_title'];
        $career->company = $input['company'];
        $career->location = $input['location'];
        $career->publish_date = date('Y-m-d', strtotime($input['publish_date']));
        $career->status = $input['status'];
        $career->updated_at = date('Y-m-d');
        $career->save();

        //$career = $this->careerRepository->update($input, $id);

        Flash::success('Career berhasil di update.');

        return redirect(route('admin.register_careers.index'));
    }

    public function getModalDelete($id = null)
      {
          //dd($id);
          $error = '';
          $model = '';
          $confirm_route =  route('admin.form_online.delete',['id'=>$id]);
          return View('admin.layouts/modal_confirmation', compact('error','model', 'confirm_route'));

      }

       public function getDelete($id = null)
       {
           $sample = FormOnline::destroy($id);
           // Redirect to the group management page
           return redirect(route('admin.form_online.index'))->with('success', Lang::get('message.success.delete'));

       }
    public function download($id) 
    {
        $data = RegisterCareer::find($id)->cv;
        $file_contents = base64_decode($data);
        return response($file_contents)
          ->header('Cache-Control', 'no-cache private')
          ->header('Content-Description', 'File Transfer')
          ->header('Content-Type', 'application/pdf')
          ->header('Content-length', strlen($file_contents))
          ->header('Content-Disposition', 'attachment; filename=' . 'nama.pdf')
          ->header('Content-Transfer-Encoding', 'binary');
    }
}
