<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\RegisterCareer;
use Flash;
use Illuminate\Support\Facades\Lang;

class RegisterCareerController extends Controller
{
    public function index(Request $request)
    {
        $register_careers = RegisterCareer::select('id','name','job_title','email','address')->get();
        
        return view('admin.register_career.index')
            ->with('register_careers', $register_careers);
    }

    public function create()
    {
        $register_career = null;
        $status = [1 => 'Aktif', 0 => 'Tidak Aktif'];
        return view('admin.register_career.create')->with(['register_career' => $register_career, 'status' => $status]);
    }

    public function store(Request $request)
    {
        $input = $request->all();
        $input['publish_date'] = date('Y-m-d', strtotime($input['publish_date']));

        Career::create($input);

        Flash::success('save success.');

        return redirect(route('admin.register_career.index'));
    }

    public function edit($id)
    {
        $career = Career::find($id);
        $status = [1 => 'Aktif', 0 => 'Tidak Aktif'];
        if (empty($career)) {
            Flash::error('Career not found');

            return redirect(route('registers_career.index'));
        }

        return view('admin.register_career.edit')->with(['register_career' => $career, 'status' => $status]);
    }

    public function update($id, Request $request)
    {
        $career = Career::find($id);

        $input = $request->all();

        if (empty($career)) {
            Flash::error('Career not found');

            return redirect(route('register_careers.index'));
        }

        $career->job_title = $input['job_title'];
        $career->company = $input['company'];
        $career->location = $input['location'];
        $career->publish_date = date('Y-m-d', strtotime($input['publish_date']));
        $career->status = $input['status'];
        $career->updated_at = date('Y-m-d');
        $career->save();

        //$career = $this->careerRepository->update($input, $id);

        Flash::success('Career berhasil di update.');

        return redirect(route('admin.register_careers.index'));
    }

    public function getModalDelete($id = null)
      {
          //dd($id);
          $error = '';
          $model = '';
          $confirm_route =  route('admin.register_careers.delete',['id'=>$id]);
          return View('admin.layouts/modal_confirmation', compact('error','model', 'confirm_route'));

      }

       public function getDelete($id = null)
       {
           $sample = RegisterCareer::destroy($id);
           // Redirect to the group management page
           return redirect(route('admin.register_careers.index'))->with('success', Lang::get('message.success.delete'));

       }
    public function download($id) 
    {
        $data = RegisterCareer::find($id)->cv;
        $file_contents = base64_decode($data);
        return response($file_contents)
          ->header('Cache-Control', 'no-cache private')
          ->header('Content-Description', 'File Transfer')
          ->header('Content-Type', 'application/pdf')
          ->header('Content-length', strlen($file_contents))
          ->header('Content-Disposition', 'attachment; filename=' . 'nama.pdf')
          ->header('Content-Transfer-Encoding', 'binary');
    }
}
