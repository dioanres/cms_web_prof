<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Service;
use Flash;
use Illuminate\Support\Facades\Lang;

class ServiceController extends Controller
{
    public function index(Request $request)
    {
        $services = Service::all();
        
        return view('admin.service.index')
            ->with('services', $services);
    }

    public function create()
    {
        $service = null;
        return view('admin.service.create')->with(['service' => $service]);
    }

    public function store(Request $request)
    {
        $input = $request->all();

        //dd($input);
        Service::create($input);
        //$jenisProdukKatalog = $this->jenisProdukKatalogRepository->create($input);

        Flash::success('save success.');

        return redirect(route('admin.services.index'));
    }

    public function edit($id)
    {
        $service = Service::find($id);
    
        if (empty($service)) {
            Flash::error('Service not found');

            return redirect(route('services.index'));
        }

        return view('admin.service.edit')->with(['service' => $service]);
    }

    public function update($id, Request $request)
    {
        $service = Service::find($id);

        $input = $request->all();

        if (empty($service)) {
            Flash::error('Service not found');

            return redirect(route('services.index'));
        }

        $service->service = $input['service'];
        $service->updated_at = date('Y-m-d');
        $service->save();

        //$career = $this->careerRepository->update($input, $id);

        Flash::success('Service berhasil di update.');

        return redirect(route('admin.services.index'));
    }

    public function getModalDelete($id = null)
      {
          $error = '';
          $model = '';
          $confirm_route =  route('admin.services.delete',['id'=>$id]);
          return View('admin.layouts/modal_confirmation', compact('error','model', 'confirm_route'));

      }

       public function getDelete($id = null)
       {
           $sample = Service::destroy($id);

           // Redirect to the group management page
           return redirect(route('admin.services.index'))->with('success', Lang::get('message.success.delete'));

       }
}
