<?php
namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Requests\CreateJenisProdukKatalogRequest;
use App\Http\Requests\UpdateJenisProdukKatalogRequest;
use App\Repositories\JenisProdukKatalogRepository;
use App\Http\Controllers\AppBaseController as InfyOmBaseController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Lang;
use App\Models\JenisProdukKatalog;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class JenisProdukKatalogController extends InfyOmBaseController
{
    /** @var  JenisProdukKatalogRepository */
    private $jenisProdukKatalogRepository;

    public function __construct(JenisProdukKatalogRepository $jenisProdukKatalogRepo)
    {
        $this->jenisProdukKatalogRepository = $jenisProdukKatalogRepo;
    }

    /**
     * Display a listing of the JenisProdukKatalog.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->jenisProdukKatalogRepository->pushCriteria(new RequestCriteria($request));
        $jenisProdukKatalogs = $this->jenisProdukKatalogRepository->all();

        return view('admin.jenisProdukKatalogs.index')
            ->with('jenisProdukKatalogs', $jenisProdukKatalogs);
    }

    /**
     * Show the form for creating a new JenisProdukKatalog.
     *
     * @return Response
     */
    public function create()
    {
        $jenisProdukKatalog = null;
        return view('admin.jenisProdukKatalogs.create')->with('jenisProdukKatalog',$jenisProdukKatalog);
    }

    /**
     * Store a newly created JenisProdukKatalog in storage.
     *
     * @param CreateJenisProdukKatalogRequest $request
     *
     * @return Response
     */
    public function store(CreateJenisProdukKatalogRequest $request)
    {
        $input = $request->all();
        if ($request->hasFile('image')){
            $file = $request->file('image');
            $extension = $file->extension()?: 'png';
            $picture = str_random(10) . '.' . $extension;
            $destinationPath = public_path() . '/uploads/jenis_produk/';
            $file->move($destinationPath, $picture);
            $input['image'] = $picture;
        }
        $jenisProdukKatalog = $this->jenisProdukKatalogRepository->create($input);

        Flash::success('Jenis Produk Katalog berhasil disimpan.');

        return redirect(route('admin.jenisProdukKatalogs.index'));
    }

    /**
     * Display the specified JenisProdukKatalog.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $jenisProdukKatalog = $this->jenisProdukKatalogRepository->findWithoutFail($id);

        if (empty($jenisProdukKatalog)) {
            Flash::error('JenisProdukKatalog not found');

            return redirect(route('jenisProdukKatalogs.index'));
        }

        return view('admin.jenisProdukKatalogs.show')->with('jenisProdukKatalog', $jenisProdukKatalog);
    }

    /**
     * Show the form for editing the specified JenisProdukKatalog.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $jenisProdukKatalog = $this->jenisProdukKatalogRepository->findWithoutFail($id);

        if (empty($jenisProdukKatalog)) {
            Flash::error('Jenis Produk Katalog not found');

            return redirect(route('jenisProdukKatalogs.index'));
        }

        return view('admin.jenisProdukKatalogs.edit')->with('jenisProdukKatalog', $jenisProdukKatalog);
    }

    /**
     * Update the specified JenisProdukKatalog in storage.
     *
     * @param  int              $id
     * @param UpdateJenisProdukKatalogRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateJenisProdukKatalogRequest $request)
    {
        $jenisProdukKatalog = $this->jenisProdukKatalogRepository->findWithoutFail($id);

        $input = $request->all();

        if (empty($jenisProdukKatalog)) {
            Flash::error('JenisProdukKatalog not found');

            return redirect(route('jenisProdukKatalogs.index'));
        }
        //dd($input,$request->all());
        if ($request->hasFile('image')){
            $file = $request->file('image');
            $extension = $file->extension()?: 'png';
            $picture = str_random(10) . '.' . $extension;
            $destinationPath = public_path() . '/uploads/jenis_produk/';
            $file->move($destinationPath, $picture);
            $input['image'] = $picture;
        }

        $jenisProdukKatalog = $this->jenisProdukKatalogRepository->update($input, $id);

        Flash::success('Jenis Produk Katalog berhasil di update.');

        return redirect(route('admin.jenisProdukKatalogs.index'));
    }

    /**
     * Remove the specified JenisProdukKatalog from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
      public function getModalDelete($id = null)
      {
          $error = '';
          $model = '';
          $confirm_route =  route('admin.jenisProdukKatalogs.delete',['id'=>$id]);
          return View('admin.layouts/modal_confirmation', compact('error','model', 'confirm_route'));

      }

       public function getDelete($id = null)
       {
           $sample = JenisProdukKatalog::destroy($id);

           // Redirect to the group management page
           return redirect(route('admin.jenisProdukKatalogs.index'))->with('success', Lang::get('message.success.delete'));

       }
}
