<?php
namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Requests\CreateKaryawanRequest;
use App\Http\Requests\UpdateKaryawanRequest;
use App\Repositories\KaryawanRepository;
use App\Http\Controllers\AppBaseController as InfyOmBaseController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Lang;
use App\Models\Karyawan;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * @author [dioanres]
 * @email [dioanres@gmail.com]
 * @create date 2020-04-10 22:54:53
 * @modify date 2020-04-10 22:54:53
 * @desc [description]
 */

class KaryawanController extends InfyOmBaseController
{
    /** @var  KaryawanRepository */
    private $karyawanRepository;

    public function __construct(KaryawanRepository $karyawanRepo)
    {
        $this->karyawanRepository = $karyawanRepo;
    }

    /**
     * Display a listing of the Karyawan.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->karyawanRepository->pushCriteria(new RequestCriteria($request));
        $karyawans = $this->karyawanRepository->all();

        return view('admin.karyawans.index')
            ->with('karyawans', $karyawans);
    }

    /**
     * Show the form for creating a new Karyawan.
     *
     * @return Response
     */
    public function create()
    {
        $karyawan = null;
        return view('admin.karyawans.create',compact('karyawan'));
    }

    /**
     * Store a newly created Karyawan in storage.
     *
     * @param CreateKaryawanRequest $request
     *
     * @return Response
     */
    public function store(CreateKaryawanRequest $request)
    {
        $input = $request->all();

        if ($request->hasFile('foto')){
            $file = $request->file('foto');
            $extension = $file->extension()?: 'png';
            $picture = str_random(10) . '.' . $extension;
            $destinationPath = public_path() . '/uploads/karyawan/';
            $file->move($destinationPath, $picture);
            $input['foto'] = $picture;
            $input['path_foto'] = '/uploads/karyawan/';
        }

        //dd($input);

        $karyawan = $this->karyawanRepository->create($input);

        Flash::success('Karyawan saved successfully.');

        return redirect(route('admin.karyawans.index'));
    }

    /**
     * Display the specified Karyawan.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $karyawan = $this->karyawanRepository->findWithoutFail($id);

        if (empty($karyawan)) {
            Flash::error('Karyawan not found');

            return redirect(route('karyawans.index'));
        }

        return view('admin.karyawans.show')->with('karyawan', $karyawan);
    }

    /**
     * Show the form for editing the specified Karyawan.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $karyawan = $this->karyawanRepository->findWithoutFail($id);

        if (empty($karyawan)) {
            Flash::error('Karyawan not found');

            return redirect(route('karyawans.index'));
        }

        return view('admin.karyawans.edit')->with('karyawan', $karyawan);
    }

    /**
     * Update the specified Karyawan in storage.
     *
     * @param  int              $id
     * @param UpdateKaryawanRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateKaryawanRequest $request)
    {
        $karyawan = $this->karyawanRepository->findWithoutFail($id);
        $input = $request->all();

        if (empty($karyawan)) {
            Flash::error('Karyawan not found');

            return redirect(route('karyawans.index'));
        }
        //dd($input,$request->hasFile('foto'));
        if ($request->hasFile('foto')){
            $file = $request->file('foto');
            $extension = $file->extension()?: 'png';
            $picture = str_random(10) . '.' . $extension;
            $destinationPath = public_path() . '/uploads/karyawan/';
            $file->move($destinationPath, $picture);
            $input['foto'] = $picture;
            $input['path_foto'] = '/uploads/karyawan/';
        }

        $karyawan = $this->karyawanRepository->update($input, $id);

        Flash::success('Karyawan updated successfully.');

        return redirect(route('admin.karyawans.index'));
    }

    /**
     * Remove the specified Karyawan from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
      public function getModalDelete($id = null)
      {
          $error = '';
          $model = '';
          $confirm_route =  route('admin.karyawans.delete',['id'=>$id]);
          return View('admin.layouts/modal_confirmation', compact('error','model', 'confirm_route'));

      }

       public function getDelete($id = null)
       {
           $sample = Karyawan::destroy($id);

           // Redirect to the group management page
           return redirect(route('admin.karyawans.index'))->with('success', Lang::get('message.success.delete'));

       }
}
