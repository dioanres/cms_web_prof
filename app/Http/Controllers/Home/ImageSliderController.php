<?php
namespace App\Http\Controllers\Home;

use App\Http\Requests\Home;
use App\Http\Requests\Home\CreateImageSliderRequest;
use App\Http\Requests\Home\UpdateImageSliderRequest;
use App\Repositories\Home\ImageSliderRepository;
use App\Repositories\PilarUsahaRepository;
use App\Http\Controllers\AppBaseController as InfyOmBaseController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Lang;
use App\Models\Home\ImageSlider;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;
/**
 * @author [dioanres]
 * @email [dioanres@gmail.com]
 * @create date 2020-04-10 11:19:32
 * @modify date 2020-04-10 11:19:32
 * @desc [description]
 */
class ImageSliderController extends InfyOmBaseController
{
    /** @var  ImageSliderRepository */
    private $imageSliderRepository;

    public function __construct(ImageSliderRepository $imageSliderRepo,
                                PilarUsahaRepository $pilarUsahaRepo)
    {
        $this->imageSliderRepository = $imageSliderRepo;
        $this->pilarUsahaRepository = $pilarUsahaRepo;
    }

    /**
     * Display a listing of the ImageSlider.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request,$type)
    {
        
        $this->imageSliderRepository->pushCriteria(new RequestCriteria($request));
        $imageSliders = $this->imageSliderRepository->getAll($type);

        return view('admin.home.imageSliders.index')
            ->with('imageSliders', $imageSliders);
    }

    public function indexBussinessUnit()
    {
        $imageSliders = $this->imageSliderRepository->getByBussinessUnit();
        //dd($imageSliders);
        return view('admin.home.imageSliders.bussiness_unit.index')
            ->with('imageSliders', $imageSliders);
    }
    /**
     * Show the form for creating a new ImageSlider.
     *
     * @return Response
     */
    public function create($type)
    {
        
        return view('admin.home.imageSliders.create');
    }

    public function createBussinessUnit()
    {   
        $pilarUsahas = $this->pilarUsahaRepository->selectPilarUsaha();
        return view('admin.home.imageSliders.bussiness_unit.create')
                ->with(['pilarUsahas' => $pilarUsahas
                ,'selected' => null]);
    }

    /**
     * Store a newly created ImageSlider in storage.
     *
     * @param CreateImageSliderRequest $request
     *
     * @return Response
     */
    public function store(CreateImageSliderRequest $request)
    {
        //dd($request->all(),$request->hasFile('image'),$request->id_pilar_usaha);
        $input = $request->all();
        
        if ($request->hasFile('image')){
            $file = $request->file('image');

            $extension = $file->extension()?: 'png';
            $picture = str_random(10) . '.' . $extension;
            $destinationPath = public_path() . '/uploads/home/slider/';
            $file->move($destinationPath, $picture);
            $input['image'] = $picture;
        }
        $imageSlider = $this->imageSliderRepository->create($input);

        Flash::success('ImageSlider saved successfully.');
        if($request->id_pilar_usaha) {
            return redirect(route('admin.home.imageSliders.bussinessUnit.index'));
        }
        return redirect(route('admin.home.imageSliders.index',$input['type']));
    }

    /**
     * Display the specified ImageSlider.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $imageSlider = $this->imageSliderRepository->findWithoutFail($id);

        if (empty($imageSlider)) {
            Flash::error('ImageSlider not found');

            return redirect(route('imageSliders.index'));
        }

        return view('admin.home.imageSliders.show')->with('imageSlider', $imageSlider);
    }

    /**
     * Show the form for editing the specified ImageSlider.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id,$type)
    {
        $imageSlider = $this->imageSliderRepository->findWithoutFail($id);
        $selected = $imageSlider->id_pilar_usaha;
        if (empty($imageSlider)) {
            Flash::error('ImageSlider not found');

            return redirect(route('imageSliders.index'));
        }
        if($type == 'bussinessUnit') {
            $pilarUsahas = $this->pilarUsahaRepository->selectPilarUsaha();
            return view('admin.home.imageSliders.bussiness_unit.edit')
                    ->with(['imageSlider' => $imageSlider,
                            'pilarUsahas' => $pilarUsahas,
                            'selected' => $selected]);
        } else {
            return view('admin.home.imageSliders.edit')->with('imageSlider', $imageSlider);
        }
        
    }

    public function editBussinessUnit($id,$type)
    {
        $imageSlider = $this->imageSliderRepository->findWithoutFail($id);

        if (empty($imageSlider)) {
            Flash::error('ImageSlider not found');

            return redirect(route('imageSliders.index'));
        }

        return view('admin.home.imageSliders.bussiness_unit.edit')->with('imageSlider', $imageSlider);
    }

    public function updateStatus($id,$status_current)
    {
        dd($id);
    }

    /**
     * Update the specified ImageSlider in storage.
     *
     * @param  int              $id
     * @param UpdateImageSliderRequest $request
     *
     * @return Response
     */
    public function update($id, CreateImageSliderRequest $request)
    {
        $imageSlider = $this->imageSliderRepository->findWithoutFail($id);
        $order   = array("\r\n", "\n", "\r");
        $replace = '<br />';

        $input = $request->all();
        $input['description'] = str_replace($order, $replace, $input['description']);
        if (empty($imageSlider)) {
            Flash::error('ImageSlider not found');

            return redirect(route('imageSliders.index'));
        }

        if ($request->hasFile('image')){
            $file = $request->file('image');

            $extension = $file->extension()?: 'png';
            $picture = str_random(10) . '.' . $extension;
            $destinationPath = public_path() . '/uploads/home/slider/';
            
            $file->move($destinationPath, $picture);
            
            $input['image'] = $picture;
            //dd($destinationPath,$picture,$request->all(),$request->image);
        } else{
            $request['image'] = $imageSlider->image;
        }
        //dd($input);

        $imageSlider = $this->imageSliderRepository->update($input, $id);

        Flash::success('ImageSlider updated successfully.');

        if($request->id_pilar_usaha) {
            return redirect(route('admin.home.imageSliders.bussinessUnit.index'));
        }

        return redirect(route('admin.home.imageSliders.index',$input['type']));
    }

    /**
     * Remove the specified ImageSlider from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
      public function getModalDelete($id = null)
      {
          $error = '';
          $model = '';
          $confirm_route =  route('admin.home.imageSliders.delete',['id'=>$id]);
          return View('admin.layouts/modal_confirmation', compact('error','model', 'confirm_route'));

      }

       public function getDelete($id = null)
       {
           $sample = ImageSlider::destroy($id);

           // Redirect to the group management page
           return redirect(route('admin.home.imageSliders.index'))->with('success', Lang::get('message.success.delete'));

       }

       public function getAktif($id = null,$active = null)
       {
           //dd($id,$active);
           $sample = ImageSlider::find($id);
           $sample->active = $active;
           $sample->save();

           // Redirect to the group management page
           return 'sukses';

       }
}
