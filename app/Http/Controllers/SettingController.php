<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Setting;
use Flash;
use Illuminate\Support\Facades\Lang;

class SettingController extends Controller
{
    public function index(Request $request)
    {
        $settings = Setting::all();
        
        return view('admin.setting.index')
            ->with('settings', $settings);
    }

    public function create()
    {
        $setting = null;
        return view('admin.setting.create')->with(['setting' => $setting]);
    }

    public function store(Request $request)
    {
        $input = $request->all();

        
        if ($request->hasFile('logo_header')){
            $file = $request->file('logo_header');
            $extension = $file->extension()?: 'png';
            $picture = str_random(10) . '.' . $extension;
            $destinationPath = public_path() . '/uploads/setting/';
            $file->move($destinationPath, $picture);
            $input['logo_header'] = $picture;
        }

        if ($request->hasFile('logo_footer')){
            $file = $request->file('logo_footer');
            $extension = $file->extension()?: 'png';
            $picture = str_random(10) . '.' . $extension;
            $destinationPath = public_path() . '/uploads/setting/';
            $file->move($destinationPath, $picture);
            $input['logo_footer'] = $picture;
        }
        //dd($input);
        Setting::create($input);

        Flash::success('save success.');

        return redirect(route('admin.setting.index'));
    }

    public function edit($id)
    {
        $setting = Setting::find($id);
        if (empty($setting)) {
            Flash::error('Setting not found');

            return redirect(route('setting.index'));
        }

        return view('admin.setting.edit')->with(['setting' => $setting]);
    }

    public function update($id, Request $request)
    {
        $setting = Setting::find($id);

        $input = $request->all();

        if (empty($setting)) {
            Flash::error('Setting not found');

            return redirect(route('setting.index'));
        }

        if ($request->hasFile('logo_header')){
            $file = $request->file('logo_header');
            $extension = $file->extension()?: 'png';
            $picture = str_random(10) . '.' . $extension;
            $destinationPath = public_path() . '/uploads/setting/';
            $file->move($destinationPath, $picture);
            $input['logo_header'] = $picture;
        }

        if ($request->hasFile('logo_footer')){
            $file = $request->file('logo_footer');
            $extension = $file->extension()?: 'png';
            $picture = str_random(10) . '.' . $extension;
            $destinationPath = public_path() . '/uploads/setting/';
            $file->move($destinationPath, $picture);
            $input['logo_footer'] = $picture;
        }

        $setting->logo_header = $input['logo_header'];
        $setting->logo_footer = $input['logo_footer'];
        $setting->updated_at = date('Y-m-d');
        $setting->meta_title = $input['meta_title'];
        $setting->copyright = $input['copyright'];
        $setting->logo_header_title = $input['logo_header_title'];
        $setting->save();

        Flash::success('Seeting berhasil di update.');

        return redirect(route('admin.setting.index'));
    }

    public function getModalDelete($id = null)
      {
          $error = '';
          $model = '';
          $confirm_route =  route('admin.careers.delete',['id'=>$id]);
          return View('admin.layouts/modal_confirmation', compact('error','model', 'confirm_route'));

      }

       public function getDelete($id = null)
       {
           $sample = Career::destroy($id);

           // Redirect to the group management page
           return redirect(route('admin.careers.index'))->with('success', Lang::get('message.success.delete'));

       }
}
