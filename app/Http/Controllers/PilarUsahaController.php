<?php
namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Requests\CreatePilarUsahaRequest;
use App\Http\Requests\UpdatePilarUsahaRequest;
use App\Repositories\PilarUsahaRepository;
use App\Http\Controllers\AppBaseController as InfyOmBaseController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Lang;
use App\Models\PilarUsaha;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class PilarUsahaController extends InfyOmBaseController
{
    /** @var  PilarUsahaRepository */
    private $pilarUsahaRepository;

    public function __construct(PilarUsahaRepository $pilarUsahaRepo)
    {
        $this->pilarUsahaRepository = $pilarUsahaRepo;
    }

    /**
     * Display a listing of the PilarUsaha.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->pilarUsahaRepository->pushCriteria(new RequestCriteria($request));
        $pilarUsahas = $this->pilarUsahaRepository->all();

        return view('admin.pilarUsahas.index')
            ->with('pilarUsahas', $pilarUsahas);
    }

    /**
     * Show the form for creating a new PilarUsaha.
     *
     * @return Response
     */
    public function create()
    {
        $pilarUsaha = null;

        return view('admin.pilarUsahas.create',compact('pilarUsaha'));
    }

    /**
     * Store a newly created PilarUsaha in storage.
     *
     * @param CreatePilarUsahaRequest $request
     *
     * @return Response
     */
    public function store(CreatePilarUsahaRequest $request)
    {
        $input = $request->all();

        if ($request->hasFile('logo')){
            $file = $request->file('logo');
            $extension = $file->extension()?: 'png';
            $picture = str_random(10) . '.' . $extension;
            $destinationPath = public_path() . '/uploads/pilar_usaha/';
            $file->move($destinationPath, $picture);
            $input['logo'] = $picture;
            $input['path_logo'] = '/uploads/pilar_usaha/';
        }

        if ($request->hasFile('logo_hover')){
            $file = $request->file('logo_hover');
            $extension = $file->extension()?: 'png';
            $picture = str_random(10) . '.' . $extension;
            $destinationPath = public_path() . '/uploads/pilar_usaha/';
            $file->move($destinationPath, $picture);
            $input['logo_hover'] = $picture;
            $input['path_logo'] = '/uploads/pilar_usaha/';
        }

        if ($request->hasFile('side_left_logo')){
            $file = $request->file('side_left_logo');
            $extension = $file->extension()?: 'png';
            $picture = str_random(10) . '.' . $extension;
            $destinationPath = public_path() . '/uploads/pilar_usaha/';
            $file->move($destinationPath, $picture);
            $input['side_left_logo'] = $picture;
            $input['path_logo'] = '/uploads/pilar_usaha/';
        }

        if ($request->hasFile('image_slider')){
            $file = $request->file('image_slider');
            $extension = $file->extension()?: 'png';
            $picture = str_random(10) . '.' . $extension;
            $destinationPath = public_path() . '/uploads/pilar_usaha/';
            $file->move($destinationPath, $picture);
            $input['image_slider'] = $picture;
            $input['path_logo'] = '/uploads/pilar_usaha/';
        }


        $pilarUsaha = $this->pilarUsahaRepository->create($input);

        Flash::success('PilarUsaha saved successfully.');

        return redirect(route('admin.pilarUsahas.index'));
    }

    /**
     * Display the specified PilarUsaha.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $pilarUsaha = $this->pilarUsahaRepository->findWithoutFail($id);

        if (empty($pilarUsaha)) {
            Flash::error('PilarUsaha not found');

            return redirect(route('pilarUsahas.index'));
        }

        return view('admin.pilarUsahas.show')->with('pilarUsaha', $pilarUsaha);
    }

    /**
     * Show the form for editing the specified PilarUsaha.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $pilarUsaha = $this->pilarUsahaRepository->findWithoutFail($id);

        if (empty($pilarUsaha)) {
            Flash::error('PilarUsaha not found');

            return redirect(route('pilarUsahas.index'));
        }
        //dd(empty($pilarUsaha->logo_hover));
        return view('admin.pilarUsahas.edit')->with('pilarUsaha', $pilarUsaha);
    }

    /**
     * Update the specified PilarUsaha in storage.
     *
     * @param  int              $id
     * @param UpdatePilarUsahaRequest $request
     *
     * @return Response
     */
    public function update($id, UpdatePilarUsahaRequest $request)
    {
        $pilarUsaha = $this->pilarUsahaRepository->findWithoutFail($id);

        $input = $request->all();
        $input['path_logo'] = '/uploads/pilar_usaha/';
        if ($request->hasFile('logo')){
            $file = $request->file('logo');
            $extension = $file->extension()?: 'png';
            $picture = str_random(10) . '.' . $extension;
            $destinationPath = public_path() . '/uploads/pilar_usaha/';
            $file->move($destinationPath, $picture);
            $input['logo'] = $picture;
        }

        if ($request->hasFile('logo_hover')){
            $file = $request->file('logo_hover');
            $extension = $file->extension()?: 'png';
            $picture = str_random(10) . '.' . $extension;
            $destinationPath = public_path() . '/uploads/pilar_usaha/';
            $file->move($destinationPath, $picture);
            $input['logo_hover'] = $picture;
        }

        if ($request->hasFile('side_left_logo')){
            $file = $request->file('side_left_logo');
            $extension = $file->extension()?: 'png';
            $picture = str_random(10) . '.' . $extension;
            $destinationPath = public_path() . '/uploads/pilar_usaha/';
            $file->move($destinationPath, $picture);
            $input['side_left_logo'] = $picture;
        }

        if ($request->hasFile('image_slider')){
            $file = $request->file('image_slider');
            $extension = $file->extension()?: 'png';
            $picture = str_random(10) . '.' . $extension;
            $destinationPath = public_path() . '/uploads/pilar_usaha/';
            $file->move($destinationPath, $picture);
            $input['image_slider'] = $picture;
        }
        
        if (empty($pilarUsaha)) {
            Flash::error('PilarUsaha not found');

            return redirect(route('pilarUsahas.index'));
        }

        $pilarUsaha = $this->pilarUsahaRepository->update($input, $id);

        Flash::success('PilarUsaha updated successfully.');

        return redirect(route('admin.pilarUsahas.index'));
    }

    /**
     * Remove the specified PilarUsaha from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
      public function getModalDelete($id = null)
      {
          $error = '';
          $model = '';
          $confirm_route =  route('admin.pilarUsahas.delete',['id'=>$id]);
          return View('admin.layouts/modal_confirmation', compact('error','model', 'confirm_route'));

      }

       public function getDelete($id = null)
       {
           $sample = PilarUsaha::destroy($id);

           // Redirect to the group management page
           return redirect(route('admin.pilarUsahas.index'))->with('success', Lang::get('message.success.delete'));

       }
}
