<?php
namespace App\Http\Controllers;

use App\DetailMisi;
use App\Http\Requests;
use App\Http\Requests\CreateContentRequest;
use App\Http\Requests\UpdateContentRequest;
use App\Repositories\ContentRepository;
use App\Http\Controllers\AppBaseController as InfyOmBaseController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Lang;
use App\Models\Content;
use App\Models\MenuUtama;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;
use Session;

class ContentController extends InfyOmBaseController
{
    /** @var  ContentRepository */
    private $contentRepository;

    public function __construct(ContentRepository $contentRepo)
    {
        $this->contentRepository = $contentRepo;
    }

    /**
     * Display a listing of the Content.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request,$id)
    {
        Session::put('menu_id',$id);
        $this->contentRepository->pushCriteria(new RequestCriteria($request));
        
        $contents= Content::where('menu_utama',$id)->get();
        
        return view('admin.contents.index')
            ->with('contents', $contents);
    }

    /**
     * Show the form for creating a new Content.
     *
     * @return Response
     */
    public function create()
    {   
        $menu_utama = MenuUtama::all();
        $content = null;
        return view('admin.contents.create',compact('menu_utama','content'));
    }

    /**
     * Store a newly created Content in storage.
     *
     * @param CreateContentRequest $request
     *
     * @return Response
     */
    public function store(CreateContentRequest $request)
    {
        $input = $request->all();
        $input['menu_utama'] = Session::get('menu_id');
        
        if ($request->hasFile('icon_hp')){
            $file = $request->file('icon_hp');
            $extension = $file->extension()?: 'png';
            $picture = str_random(10) . '.' . $extension;
            $destinationPath = public_path() . '/uploads/content/';
            $file->move($destinationPath, $picture);
            $input['icon_hp'] = $picture;
        }

        $content = $this->contentRepository->create($input);

        Flash::success('Content saved successfully.');

        return redirect(route('admin.contents.index',['id' => Session::get('menu_id')]));
    }

    /**
     * Display the specified Content.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $content = $this->contentRepository->findWithoutFail($id);
        $menu_utama = MenuUtama::all();

        if (empty($content)) {
            Flash::error('Content not found');

            return redirect(route('contents.index'));
        }

        return view('admin.contents.show')->with(['content' => $content,'menu_utama' => $menu_utama]);
    }

    /**
     * Show the form for editing the specified Content.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $content = $this->contentRepository->findWithoutFail($id);
        $misi = DetailMisi::where('id_content',$id)->get();
        $menu_utama = MenuUtama::all();

        if (empty($content)) {
            Flash::error('Content not found');

            return redirect(route('contents.index'));
        }
        
        return view('admin.contents.edit')->with(['content'=> $content,'menu_utama' => $menu_utama,'misi' =>$misi]);
    }

    /**
     * Update the specified Content in storage.
     *
     * @param  int              $id
     * @param UpdateContentRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateContentRequest $request)
    {
        $content = $this->contentRepository->findWithoutFail($id);
        $input = $request->all();
        
        if ($request->hasFile('icon_hp')){
            $file = $request->file('icon_hp');
            $extension = $file->extension()?: 'png';
            $picture = str_random(10) . '.' . $extension;
            $destinationPath = public_path() . '/uploads/content/';
            $file->move($destinationPath, $picture);
            $input['icon_hp'] = $picture;
        }

        if ($request->hasFile('icon_office')){
            $file = $request->file('icon_office');
            $extension = $file->extension()?: 'png';
            $picture = str_random(10) . '.' . $extension;
            $destinationPath = public_path() . '/uploads/content/';
            $file->move($destinationPath, $picture);
            $input['icon_office'] = $picture;
        }

        if ($request->hasFile('icon_form')){
            $file = $request->file('icon_form');
            $extension = $file->extension()?: 'png';
            $picture = str_random(10) . '.' . $extension;
            $destinationPath = public_path() . '/uploads/content/';
            $file->move($destinationPath, $picture);
            $input['icon_form'] = $picture;
        }

        $input['path'] = '/uploads/content/';
        
        if (empty($content)) {
            Flash::error('Content not found');

            return redirect(route('contents.index'));
        }
        
        $content = $this->contentRepository->update($input, $id);

        Flash::success('Content updated successfully.');
        return redirect(route('admin.contents.index',['id' => Session::get('menu_id')]));
    }

    /**
     * Remove the specified Content from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
      public function getModalDelete($id = null)
      {
          $error = '';
          $model = '';
          $confirm_route =  route('admin.contents.delete',['id'=>$id]);
          return View('admin.layouts/modal_confirmation', compact('error','model', 'confirm_route'));
      }

       public function getDelete($id = null)
       {
           $sample = Content::destroy($id);
           // Redirect to the group management page
           return redirect(route('admin.contents.index',['id' => Session::get('menu_id')]))->with('success', Lang::get('message.success.delete'));
       }

}