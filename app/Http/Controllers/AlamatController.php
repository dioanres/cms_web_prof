<?php
namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Requests\CreateAlamatRequest;
use App\Http\Requests\UpdateAlamatRequest;
use App\Repositories\AlamatRepository;
use App\Http\Controllers\AppBaseController as InfyOmBaseController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Lang;
use App\Models\Alamat;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class AlamatController extends InfyOmBaseController
{
    /** @var  AlamatRepository */
    private $alamatRepository;

    public function __construct(AlamatRepository $alamatRepo)
    {
        $this->alamatRepository = $alamatRepo;
    }

    /**
     * Display a listing of the Alamat.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->alamatRepository->pushCriteria(new RequestCriteria($request));
        $alamats = $this->alamatRepository->all();

        return view('admin.alamats.index')
            ->with('alamats', $alamats);
    }

    /**
     * Show the form for creating a new Alamat.
     *
     * @return Response
     */
    public function create()
    {
        return view('admin.alamats.create');
    }

    /**
     * Store a newly created Alamat in storage.
     *
     * @param CreateAlamatRequest $request
     *
     * @return Response
     */
    public function store(CreateAlamatRequest $request)
    {
        $input = $request->all();

        $alamat = $this->alamatRepository->create($input);

        Flash::success('Alamat saved successfully.');

        return redirect(route('admin.alamats.index'));
    }

    /**
     * Display the specified Alamat.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $alamat = $this->alamatRepository->findWithoutFail($id);

        if (empty($alamat)) {
            Flash::error('Alamat not found');

            return redirect(route('alamats.index'));
        }

        return view('admin.alamats.show')->with('alamat', $alamat);
    }

    /**
     * Show the form for editing the specified Alamat.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $alamat = $this->alamatRepository->findWithoutFail($id);

        if (empty($alamat)) {
            Flash::error('Alamat not found');

            return redirect(route('alamats.index'));
        }

        return view('admin.alamats.edit')->with('alamat', $alamat);
    }

    /**
     * Update the specified Alamat in storage.
     *
     * @param  int              $id
     * @param UpdateAlamatRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateAlamatRequest $request)
    {
        $alamat = $this->alamatRepository->findWithoutFail($id);

        

        if (empty($alamat)) {
            Flash::error('Alamat not found');

            return redirect(route('alamats.index'));
        }

        $alamat = $this->alamatRepository->update($request->all(), $id);

        Flash::success('Alamat updated successfully.');

        return redirect(route('admin.alamats.index'));
    }

    /**
     * Remove the specified Alamat from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
      public function getModalDelete($id = null)
      {
          $error = '';
          $model = '';
          $confirm_route =  route('admin.alamats.delete',['id'=>$id]);
          return View('admin.layouts/modal_confirmation', compact('error','model', 'confirm_route'));

      }

       public function getDelete($id = null)
       {
           $sample = Alamat::destroy($id);

           // Redirect to the group management page
           return redirect(route('admin.alamats.index'))->with('success', Lang::get('message.success.delete'));

       }
}
