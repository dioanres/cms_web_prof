<?php

namespace App\Http\Requests\Home;

use Illuminate\Foundation\Http\FormRequest;
use App\Models\Home\ImageSlider;

class CreateImageSliderRequest extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required',
            //'image' => 'required|mimes:jpg,jpeg,bmp,png,gif|max:20000'

        ];
    }
}
