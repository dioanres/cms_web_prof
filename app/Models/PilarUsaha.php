<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;



class PilarUsaha extends Model
{

    public $table = 'pilar_Usaha';

    public $fillable = [
        'nama_pilar_usaha',
        'judul_deskripsi',
        'deskripsi',
        'value_pilar_usaha',
        'logo',
        'logo_hover',
        'alamat',
        'no_telp',
        'no_hp',
        'keterangan',
        'path_logo',
        'url_youtube',
        'side_left_logo',
        'title_slide',
        'sub_title_slide',
        'image_slider'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'nama_pilar_usaha' => 'string',
        'judul_deskripsi' => 'string',
        'deskripsi' => 'string',
        'value_pilar_usaha' => 'string',
        'logo' => 'string',
        'logo_hover' => 'string',
        'alamat' => 'string',
        'no_telp' => 'string',
        'no_hp' => 'string',
        'keterangan' => 'string',
        'path_logo' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    public function visi_misi() {
        return $this->hasOne(\App\Models\Visimisi::class,'id_pilar_usaha');
    }

    public function catalog() {
        return $this->hasMany(\App\Models\CatalogPilarUsaha::class,'id_pilar_usaha');
    }
}
