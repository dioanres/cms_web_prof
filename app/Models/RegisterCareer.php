<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RegisterCareer extends Model
{
    public $table = 'register_career';

    public $fillable = [
        'name',
        'job_title',
        'email',
        'address',
        'cv'
    ];
}
