<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;



class Alamat extends Model
{

    public $table = 'alamats';
    


    public $fillable = [
        'alamat'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'alamat' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];
}
