<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SocialMedia extends Model
{
    public $table = 'social_media';

    public $fillable = [
        'name',
        'icon',
        'path',
        'url'
    ];
}
