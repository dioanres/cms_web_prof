<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Career extends Model
{
    public $table = 'careers';

    public $fillable = [
        'job_title',
        'company',
        'location',
        'publish_date',
        'status'
    ];
}
