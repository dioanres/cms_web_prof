<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;



class ValueCompany extends Model
{

    public $table = 'valueCompanys';
    


    public $fillable = [
        'value',
        'deskripsi_value'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'value' => 'string',
        'deskripsi_value' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];
}
