<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;



class MenuUtama extends Model
{

    public $table = 'menu_utamas';
    


    public $fillable = [
        'id',
        'nama_menu'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'nama_menu' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'nama_menu' => 'required'
    ];

    public function content()
    {
        //return $this->belongs('App\Content');
        return $this->belongsTo('App\Models\Content');
    }
}
