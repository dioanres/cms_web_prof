<?php

namespace App\Models\Home;

use Illuminate\Database\Eloquent\Model;



class ImageSlider extends Model
{

    public $table = 'image_sliders';
    


    public $fillable = [
        'image',
        'title',
        'active',
        'sub_title',
        'description',
        'type',
        'id_pilar_usaha'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'image' => 'string',
        'title' => 'string',
        'active' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    public function pilar_usaha()
     {
         return $this->hasOne(\App\Models\PilarUsaha::class,'id','id_pilar_usaha');
     }
}
