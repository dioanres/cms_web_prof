<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;



class Karyawan extends Model
{

    public $table = 'karyawans';
    


    public $fillable = [
        'nama',
        'jabatan',
        'catatan',
        'foto',
        'ig',
        'twitter',
        'fb',
        'linkedin',
        'path_foto'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'nama' => 'string',
        'jabatan' => 'string',
        'catatan' => 'string',
        'foto' => 'string',
        'ig' => 'string',
        'twitter' => 'string',
        'fb' => 'string',
        'linkedin' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];
}
