<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;



class Content extends Model
{

    public $table = 'contents';
    

    public $fillable = [
        'menu_utama',
        'title',
        'content',
        'content_bold',
        'image',
        'url_video',
        'visi',
        'misi',
        'no_hp',
        'no_telp',
        'email',
        'ig',
        'twitter',
        'facebook',
        'value',
        'alamat',
        'citizen_country',
        'citizen_information',
        'icon_hp',
        'icon_office',
        'icon_form',
        'path'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'menu_utama' => 'integer',
        'title' => 'string',
        'content' => 'string',
        'image' => 'string',
        'url_video' => 'string',
        'visi' => 'string',
        'misi' => 'string',
        'no_hp' => 'string',
        'no_telp' => 'string',
        'email' => 'string',
        'ig' => 'string',
        'twitter' => 'string',
        'facebook' => 'string',
        'value' => 'string',
        'alamat' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    public function menu_utamas()
    {
        return $this->belongsTo('App\Models\MenuUtama', 'menu_utama', 'id');
    }
}
