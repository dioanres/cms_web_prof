<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;



class Visimisi extends Model
{

    public $table = 'visimisis';
    


    public $fillable = [
        'id_pilar_usaha',
        'visi',
        'misi'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id_pilar_usaha' => 'integer',
        'visi' => 'string',
        'misi' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'id_pilar_usaha' => 'required'
    ];

    public function pilar_usaha() {
        return $this->belongsTo(\App\Models\PilarUsaha::class,'id_pilar_usaha','id');
    }
}
