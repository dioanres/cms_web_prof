<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class FormOnline extends Model
{
    public $table = 'form_online';

    public $timestamps = true;

    public $fillable = [
        'name',
        'email',
        'message',
        'created_at'
    ];
}
