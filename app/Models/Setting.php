<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Setting extends Model
{
    public $table = 'setting';

    public $fillable = [
        'logo_header',
        'logo_footer',
        'meta_title',
        'copyright',
        'logo_header_title'
    ];
}
