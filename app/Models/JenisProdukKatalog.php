<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;



class JenisProdukKatalog extends Model
{

    public $table = 'jenis_produk_katalogs';
    


    public $fillable = [
        'nama_jenis_produk','image'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'nama_jenis_produk' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    public function catalog()
	{
		return $this->belongsTo(\App\Models\CatalogPilarUsaha::class);
    }
}
