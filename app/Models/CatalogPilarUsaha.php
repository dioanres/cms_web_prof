<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class CatalogPilarUsaha extends Model
{

    public $table = 'katalog_pilar_usahas';
    


    public $fillable = [
        'id_pilar_usaha',
        'image',
        'harga',
        'deskripsi',
        'jenis_produk'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id_pilar_usaha' => 'integer',
        'image' => 'string',
        'harga' => 'float',
        'deskripsi' => 'string',
        'jenis_produk' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    public function jenis_produk()
	{
		return $this->hasOne(\App\Models\JenisProdukKatalog::class,'id','jenis_produk');
    }

    public function pilar_usaha()
    {
        return $this->belongsTo(\App\Models\PilarUsaha::class);
    }
}
