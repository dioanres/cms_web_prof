<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DetailMisi extends Model
{
    public $table = 'detail_misi';
    


    public $fillable = [
        'id_content',
        'misi'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id_content' => 'integer',
        'misi' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];
}
