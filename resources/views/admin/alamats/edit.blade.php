@extends('admin/layouts/default')

@section('title')
Alamats
@parent
@stop

@section('content')
  @include('core-templates::common.errors')
    <section class="content-header">
     <h1>Alamats Edit</h1>
     <ol class="breadcrumb">
         <li>
             <a href="{{ route('admin.dashboard') }}"> <i class="material-icons text-primary leftsize">home</i>
                 Dashboard
             </a>
         </li>
         <li>Alamats</li>
         <li class="active">Edit Alamats </li>
     </ol>
    </section>
    <section class="content paddingleft_right15">
      <div class="row">
      <div class="panel panel-primary">
            <div class="panel-heading">
                <h4 class="panel-title"> <i class="material-icons text-primary leftsize">edit</i>
                    Edit  Alamat
                </h4></div>
            <br />
        <div class="panel-body">
        {!! Form::model($alamat, ['route' => ['admin.alamats.update', collect($alamat)->first() ], 'method' => 'patch']) !!}

        @include('admin.alamats.fields')

        {!! Form::close() !!}
        </div>
      </div>
    </div>
   </section>
 @stop
