<?php
$cls = '';

if(empty($setting)) {
    $cls = 'new';
    $cls_footer = 'new';
} else {
    if(!empty($setting->logo_header)) {
        $cls = 'exists';
    } else {
        $cls = 'new';
    }
    if(!empty($setting->logo_footer)) {
        $cls_footer = 'exists';
    } else {
        $cls_footer = 'new';
    }
}

?>

<div class="form-group col-sm-12">
    {!! Form::label('meta_title', 'Meta Title:') !!}
    {!! Form::text('meta_title', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group col-sm-12">
    {!! Form::label('copyright', '&copy;Copyright:') !!}
    {!! Form::text('copyright', null, ['class' => 'form-control']) !!}
</div>
<!-- Image Field -->

<div class="form-group col-sm-12">
{!! Form::label('logo_header', 'Logo Header:') !!}
<br/>
    <div class="fileinput fileinput-<?= $cls ?>" data-provides="fileinput">
        <div class="fileinput-new thumbnail">
        </div>

        <div class="fileinput-preview fileinput-exists thumbnail">
            @if(!empty($setting))
            <img src="{!! URL::to('uploads/setting/'.$setting->logo_header) !!}">
            <input type="text" hidden name="logo_header" value="{{ $setting->logo_header }}">
            @endif
        </div>
        <div>
            <span class="btn btn-primary btn-file">
                <span class="fileinput-new">Select image</span>
                <span class="fileinput-exists">Change</span>
                <input type="file" name="logo_header">
            </span>
            <a href="#" class="btn btn-primary fileinput-exists" data-dismiss="fileinput">Remove</a>
        </div>
    </div>
</div>

<div class="form-group col-sm-12">
    {!! Form::label('logo_header_title', 'Logo Header Title') !!}
    {!! Form::text('logo_header_title', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group col-sm-12">
{!! Form::label('logo_footer', 'Logo Footer:') !!}
<br/>
    <div class="fileinput fileinput-<?= $cls ?>" data-provides="fileinput">
        <div class="fileinput-new thumbnail">
        </div>

        <div class="fileinput-preview fileinput-exists thumbnail">
            @if(!empty($setting))
            <img src="{!! URL::to('uploads/setting/'.$setting->logo_footer) !!}">
            <input type="text" hidden name="logo_footer" value="{{ $setting->logo_footer }}">
            @endif
        </div>
        <div>
            <span class="btn btn-primary btn-file">
                <span class="fileinput-new">Select image</span>
                <span class="fileinput-exists">Change</span>
                <input type="file" name="logo_footer">
            </span>
            <a href="#" class="btn btn-primary fileinput-exists" data-dismiss="fileinput">Remove</a>
        </div>
    </div>
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12 text-center">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('admin.careers.index') !!}" class="btn btn-default">Cancel</a>
</div>
