<style>
    .img-tumbnail {
        padding: 4px;
        line-height: 1.42857;
        background-color: #fff;
        border: 1px solid #ddd;
        border-radius: 3px;
        -webkit-transition: all .2s ease-in-out;
        -o-transition: all .2s ease-in-out;
        transition: all .2s ease-in-out;
        display: inline-block;
        max-width: 100%;
        height: auto;
    }
</style>

<table class="table table-responsive table-striped table-bordered" id="careers-table" width="100%">
    <thead>
     <tr>
        <th>Meta Title</th>
        <th>&copy; copyright</th>
        <th>Logo Header</th>
        <th>Logo Footer</th>
        <th>Action</th>
     </tr>
    </thead>
    <tbody>
    @foreach($settings as $setting)
        <tr>
            <td>{{ $setting->meta_title }}</td>
            <td>{{ $setting->copyright }}</td>
            <td><img src="{!! URL::to('uploads/setting/'.$setting->logo_header) !!}" width="150px" height="150px"></td>
            <td><img src="{!! URL::to('uploads/setting/'.$setting->logo_footer) !!}" width="150px" height="150px"></td>
            <td>
                 <a href="{{ route('admin.setting.edit', collect($setting)->first() ) }}">
                     <button type="button" class="btn btn-warning btn_sizes">Edit</button>
                 </a>
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
@section('footer_scripts')
    <div class="modal fade" id="delete_confirm" tabindex="-1" role="dialog" aria-labelledby="user_delete_confirm_title" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
            </div>
        </div>
    </div>
    <script>$(function () {$('body').on('hidden.bs.modal', '.modal', function () {$(this).removeData('bs.modal');});});</script>
<link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/buttons.bootstrap.css') }}"/>
<link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/dataTables.bootstrap.css') }}"/>
<link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/buttons.bootstrap.css') }}">
<script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/jquery.dataTables.js') }}" ></script>
<script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.bootstrap.js') }}" ></script>

    <script>
        $('#careers-table').DataTable({
                      responsive: true,
                      pageLength: 10
        });
         $('#careers-table').on( 'length.dt', function ( e, settings, len ) {
                     setTimeout(function(){
                         $('.livicon').updateLivicon();
                     },200);
         });

       </script>
@stop
