@extends('admin/layouts/default')

@section('title')
Setting
@parent
@stop

@section('content')
  @include('core-templates::common.errors')
    <section class="content-header">
     <h1>Setting Edit</h1>
     <ol class="breadcrumb">
         <li>
             <a href="{{ route('admin.dashboard') }}"> <i class="material-icons text-primary leftsize">home</i>
                 Dashboard
             </a>
         </li>
         <li>Career</li>
         <li class="active">Edit Setting </li>
     </ol>
    </section>
    <section class="content paddingleft_right15">
      <div class="row">
      <div class="panel panel-primary">
            <div class="panel-heading">
                <h4 class="panel-title"> <i class="material-icons text-primary leftsize">edit</i>
                    Edit  Setting
                </h4></div>
            <br />
        <div class="panel-body">
        {!! Form::model($setting, ['route' => ['admin.setting.update', collect($setting)->first() ], 'method' => 'patch','files' => true]) !!}

        @include('admin.setting.fields')

        {!! Form::close() !!}
        </div>
      </div>
    </div>
   </section>
 @stop
