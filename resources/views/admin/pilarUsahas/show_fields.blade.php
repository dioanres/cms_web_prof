
<!-- Nama Pilar Usaha Field -->
<div class="form-group">
    {!! Form::label('nama_pilar_usaha', 'Nama Pilar Usaha:') !!}
    <p>{!! $pilarUsaha->nama_pilar_usaha !!}</p>
    <hr>
</div>

<!-- Judul Deskripsi Field -->
<div class="form-group">
    {!! Form::label('judul_deskripsi', 'Judul Deskripsi:') !!}
    <p>{!! $pilarUsaha->judul_deskripsi !!}</p>
    <hr>
</div>

<!-- Deskripsi Field -->
<div class="form-group">
    {!! Form::label('deskripsi', 'Deskripsi:') !!}
    <p>{!! $pilarUsaha->deskripsi !!}</p>
    <hr>
</div>

<!-- Value Pilar Usaha Field -->
<div class="form-group">
    {!! Form::label('value_pilar_usaha', 'Value Pilar Usaha:') !!}
    <p>{!! $pilarUsaha->value_pilar_usaha !!}</p>
    <hr>
</div>

<!-- Logo Field -->
<div class="form-group">
    {!! Form::label('logo', 'Logo:') !!}
    <br/>
    <img src="{!! URL::to('uploads/pilar_usaha/'.$pilarUsaha->logo) !!}" width="200px" height="150px">
    <hr>
</div>

<!-- Alamat Field -->
<div class="form-group">
    {!! Form::label('alamat', 'Alamat:') !!}
    <p>{!! $pilarUsaha->alamat !!}</p>
    <hr>
</div>

<!-- No Telp Field -->
<div class="form-group">
    {!! Form::label('no_telp', 'No Telp:') !!}
    <p>{!! $pilarUsaha->no_telp !!}</p>
    <hr>
</div>

<!-- No Hp Field -->
<div class="form-group">
    {!! Form::label('no_hp', 'No Hp:') !!}
    <p>{!! $pilarUsaha->no_hp !!}</p>
    <hr>
</div>

<!-- Keterangan Field -->
<div class="form-group">
    {!! Form::label('keterangan', 'Keterangan:') !!}
    <p>{!! $pilarUsaha->keterangan !!}</p>
    <hr>
</div>

