<table class="table table-responsive table-striped table-bordered" id="pilarUsahas-table" width="100%">
    <thead>
     <tr>
        <th>Bussiness Unit Name</th>
        <th>Title Description</th>
        <th>Description</th>
        <th>Logo</th>
        <th>Logo Hover</th>
        <th>Address</th>
        <th>Phone No.</th>
        <th>Hp No.</th>
        <th>Notes</th>
        <th>Action</th>
     </tr>
    </thead>
    <tbody>
    @foreach($pilarUsahas as $pilarUsaha)
        <tr>
            <td>{!! $pilarUsaha->nama_pilar_usaha !!}</td>
            <td>{!! $pilarUsaha->judul_deskripsi !!}</td>
            <td>{!! $pilarUsaha->deskripsi !!}</td>
            <td>
                @if (!file_exists(URL::to('uploads/pilar_usaha/'.$pilarUsaha->logo)))
                <img src="{!! URL::to('uploads/pilar_usaha/'.$pilarUsaha->logo) !!}" width="200px" height="150px">
                @else
                <img src="{!! URL::to('assets/images/authors/no_avatar.jpg') !!}" width="200px" height="150px">
                @endif
                
            </td>
            <td>
                @if (!file_exists(URL::to('uploads/pilar_usaha/'.$pilarUsaha->logo_hover)))
                    <img src="{!! URL::to('uploads/pilar_usaha/'.$pilarUsaha->logo_hover) !!}" width="200px" height="150px">
                @else
                    <img src="{!! URL::to('assets/images/authors/no_avatar.jpg') !!}" width="200px" height="150px">
                @endif
            </td>
            <td>{!! $pilarUsaha->alamat !!}</td>
            <td>{!! $pilarUsaha->no_telp !!}</td>
            <td>{!! $pilarUsaha->no_hp !!}</td>
            <td>{!! $pilarUsaha->keterangan !!}</td>
            <td>
                 <a href="{{ route('admin.pilarUsahas.edit', collect($pilarUsaha)->first() ) }}">
                    <button type="button" class="btn btn-warning btn-xs">Edit</button>
                 </a>
                 <a href="{{ route('admin.pilarUsahas.confirm-delete', collect($pilarUsaha)->first() ) }}" data-toggle="modal" data-target="#delete_confirm">
                    <button type="button" class="btn btn-danger btn-xs">Delete</button>
                 </a>
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
@section('footer_scripts')
    <div class="modal fade" id="delete_confirm" tabindex="-1" role="dialog" aria-labelledby="user_delete_confirm_title" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
            </div>
        </div>
    </div>
    <script>$(function () {$('body').on('hidden.bs.modal', '.modal', function () {$(this).removeData('bs.modal');});});</script>
<link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/buttons.bootstrap.css') }}"/>
<link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/dataTables.bootstrap.css') }}"/>
 <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/buttons.bootstrap.css') }}">
<script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/jquery.dataTables.js') }}" ></script>
 <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.bootstrap.js') }}" ></script>

    <script>
        $('#pilarUsahas-table').DataTable({
                      responsive: true,
                      pageLength: 10
        });
         $('#pilarUsahas-table').on( 'length.dt', function ( e, settings, len ) {
                     setTimeout(function(){
                         $('.livicon').updateLivicon();
                     },200);
         });

       </script>
@stop
