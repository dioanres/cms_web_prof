<?php
$cls = '';
$cls_hover = '';
$cls_side_logo = '';
$cls_slider = '';
if(empty($pilarUsaha)) {
    $cls = 'new';
} else {
    if(!empty($pilarUsaha->logo)) {
        $cls = 'exists';
    } else {
        $cls = 'new';
    }
}

if(empty($pilarUsaha)) {
    $cls_hover = 'new';
} else {
    if(!empty($pilarUsaha->logo_hover)) {
        $cls_hover = 'exists';
    } else {
        $cls_hover = 'new';
    }
}

if(empty($pilarUsaha)) {
    $cls_side_logo = 'new';
    $cls_slider = 'new';
} else {
    if(!empty($pilarUsaha->side_left_logo)) {
        $cls_side_logo = 'exists';
    } else {
        $cls_side_logo = 'new';
    }

    if(!empty($pilarUsaha->image_slider)) {
        $cls_slider = 'exists';
    } else {
        $cls_slider = 'new';
    }
}
?>
<!-- Nama Pilar Usaha Field -->
<div class="form-group col-sm-12">
    {!! Form::label('nama_pilar_usaha', 'Bussiness Unit Name:') !!}
    {!! Form::text('nama_pilar_usaha', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group col-sm-12">
    {!! Form::label('title_header', 'Title Header:') !!}
    {!! Form::text('title_slide', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group col-sm-12">
    {!! Form::label('sub_title_header', 'Sub Title Header:') !!}
    {!! Form::textarea('sub_title_slide', null, ['class' => 'form-control']) !!}
</div>

<!-- Judul Deskripsi Field -->
<div class="form-group col-sm-12">
    {!! Form::label('judul_deskripsi', 'Description Title:') !!}
    {!! Form::text('judul_deskripsi', null, ['class' => 'form-control']) !!}
</div>

<!-- Deskripsi Field -->

<div class="form-group col-sm-12">
    <div class="panel panel-primary">
        <div class="panel-heading">
            <div class="text-muted bootstrap-admin-box-title editor-clr">
                <h3 class="panel-title">
                    Description</h3>
            </div>
        </div>
        <div class="bootstrap-admin-panel-content">
            <textarea id="ckeditor_full" class="ckeditor_full"
                name="deskripsi">{{ $pilarUsaha != null ? $pilarUsaha->deskripsi : '' }}</textarea>
        </div>
    </div>
</div>

<div class="form-group col-sm-12">
    {!! Form::label('Image Slider', 'Image Slider:') !!}
    <br/>
    <br/>
    <span>type image .png (1920x877)</span>
    <br />
    <div class="fileinput fileinput-<?= $cls_slider ?>" data-provides="fileinput">
        <div class="fileinput-new thumbnail">
        </div>

        <div class="fileinput-preview fileinput-exists thumbnail">
            @if(!empty($pilarUsaha))
            <img src="{!! URL::to('uploads/pilar_usaha/'.$pilarUsaha->image_slider) !!}">
            <input type="text" hidden name="image_slider" value="{{ $pilarUsaha->image_slider }}">
            @endif
        </div>
        <div>
            <span class="btn btn-primary btn-file">
                <span class="fileinput-new">Select image</span>
                <span class="fileinput-exists">Change</span>
                <input type="file" name="image_slider">
            </span>
            <a href="#" class="btn btn-primary fileinput-exists" data-dismiss="fileinput">Remove</a>
        </div>
    </div>
</div>

<!-- Logo Field -->

<div class="form-group col-sm-12">
    {!! Form::label('Logo', 'Logo:') !!}
    <br/>
    <br/>
    <span>type image .png (120x120)</span>
    <br />
    <div class="fileinput fileinput-<?= $cls ?>" data-provides="fileinput">

        <div class="fileinput-new thumbnail">
        </div>

        <div class="fileinput-preview fileinput-exists thumbnail">
            @if(!empty($pilarUsaha))
            <img src="{!! URL::to('uploads/pilar_usaha/'.$pilarUsaha->logo) !!}">
            <input type="text" hidden name="logo" value="{{ $pilarUsaha->logo }}">
            @endif
        </div>
        <div>
            <span class="btn btn-primary btn-file">
                <span class="fileinput-new">Select image</span>
                <span class="fileinput-exists">Change</span>
                <input type="file" name="logo">
            </span>
            <a href="#" class="btn btn-primary fileinput-exists" data-dismiss="fileinput">Remove</a>
        </div>
    </div>
</div>

<div class="form-group col-sm-12">
    {!! Form::label('Logo Hover', 'Logo Hover:') !!}
    <br/>
    <br/>
    <span>type image .png (120x120)</span>
    <br />
    <div class="fileinput fileinput-<?= $cls_hover ?>" data-provides="fileinput">

        <div class="fileinput-new thumbnail">
        </div>

        <div class="fileinput-preview fileinput-exists thumbnail">
            @if(!empty($pilarUsaha))
            <img src="{!! URL::to('uploads/pilar_usaha/'.$pilarUsaha->logo_hover) !!}">
            <input type="text" hidden name="logo_hover" value="{{ $pilarUsaha->logo_hover }}">
            @endif
        </div>
        <div>
            <span class="btn btn-primary btn-file">
                <span class="fileinput-new">Select image</span>
                <span class="fileinput-exists">Change</span>
                <input type="file" name="logo_hover">
            </span>
            <a href="#" class="btn btn-primary fileinput-exists" data-dismiss="fileinput">Remove</a>
        </div>
    </div>
</div>

<div class="clearfix"></div>

<div class="form-group col-sm-12">
    {!! Form::label('Side Left Logo', 'Side Left Logo:') !!}
    <br/>
    <br/>
    <span>type image .png</span>
    <br />
    <div class="fileinput fileinput-<?= $cls_side_logo ?>" data-provides="fileinput">

        <div class="fileinput-new thumbnail">
        </div>

        <div class="fileinput-preview fileinput-exists thumbnail">
            @if(!empty($pilarUsaha))
            <img src="{!! URL::to('uploads/pilar_usaha/'.$pilarUsaha->side_left_logo) !!}">
            <input type="text" hidden name="side_left_logo" value="{{ $pilarUsaha->side_left_logo }}">
            @endif
        </div>
        <div>
            <span class="btn btn-primary btn-file">
                <span class="fileinput-new">Select image</span>
                <span class="fileinput-exists">Change</span>
                <input type="file" name="side_left_logo">
            </span>
            <a href="#" class="btn btn-primary fileinput-exists" data-dismiss="fileinput">Remove</a>
        </div>
    </div>
</div>

<div class="clearfix"></div>

<!-- Alamat Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('alamat', 'Alamat:') !!}
    {!! Form::textarea('alamat', null, ['class' => 'form-control', 'rows' => '5']) !!}
</div>


<!-- No Telp Field -->
<div class="form-group col-sm-12">
    {!! Form::label('no_telp', 'No Telp:') !!}
    {!! Form::text('no_telp', null, ['class' => 'form-control']) !!}
</div>

<!-- No Hp Field -->
<div class="form-group col-sm-12">
    {!! Form::label('no_hp', 'No Hp:') !!}
    {!! Form::text('no_hp', null, ['class' => 'form-control']) !!}
</div>

<!-- No Hp Field -->
<div class="form-group col-sm-12">
    {!! Form::label('url_youtube', 'URL Youtube:') !!}
    {!! Form::text('url_youtube', null, ['class' => 'form-control']) !!}
</div>

<!-- Keterangan Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('keterangan', 'Keterangan:') !!}
    {!! Form::textarea('keterangan', null, ['class' => 'form-control', 'rows' => '5']) !!}
</div>


<!-- Submit Field -->
<div class="form-group col-sm-12 text-center">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('admin.pilarUsahas.index') !!}" class="btn btn-default">Cancel</a>
</div>