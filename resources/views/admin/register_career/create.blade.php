@extends('admin/layouts/default')

@section('title')
Career
@parent
@stop

@section('content')
@include('core-templates::common.errors')
<section class="content-header">
    <h1>List Career</h1>
    <ol class="breadcrumb">
        <li>
            <a href="{{ route('admin.dashboard') }}"> <i class="material-icons text-primary leftsize">home</i>
                Dashboard
            </a>
        </li>
        <li>Jenis Career</li>
        <li class="active">Add Career </li>
    </ol>
</section>
<section class="content paddingleft_right15">
<div class="row">
 <div class="panel panel-primary">
        <div class="panel-heading">
            <h4 class="panel-title"> <i class="material-icons text-primary leftsize">add_circle</i>
                New Career
            </h4></div>
        <br />
        <div class="panel-body">
        {!! Form::open(['route' => 'admin.careers.store','files' => true]) !!}

            @include('admin.career.fields')

        {!! Form::close() !!}
    </div>
  </div>
 </div>
</section>
 @stop
