<?php
$cls = '';

// if(empty($jenisProdukKatalog)) {
//     $cls = 'new';
// } else {
//     if(!empty($jenisProdukKatalog->image)) {
//         $cls = 'exists';
//     } else {
//         $cls = 'new';
//     }
// }

?>
<!-- Nama Jenis Produk Field -->
<div class="form-group col-sm-12">
    {!! Form::label('job_title', 'Job Title:') !!}
    {!! Form::text('job_title', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group col-sm-12">
    {!! Form::label('company', 'Company:') !!}
    {!! Form::text('company', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group col-sm-12">
    {!! Form::label('location', 'Location:') !!}
    {!! Form::text('location', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group col-sm-12">
    {!! Form::label('publish_date', 'Post Date:') !!}
    <div class="input-group">
        <div class="input-group-addon">
            <i class="material-icons date_pick">today</i>
        </div>
        <!-- <input type="text" class="form-control" id="rangepicker4" /> -->
        <input type="text" class="form-control datepicker" name="publish_date" value="{!! @$career->publish_date !!}" />
    </div>
    
</div>

<div class="form-group col-sm-12">
    {!! Form::label('status', 'Status:') !!}
    {!! Form::select('status', $status, null, ['class' => 'form-control']) !!}
</div>

<!-- Image Field -->
<!-- <div class="form-group col-sm-12">
    {!! Form::label('image', 'Image:') !!}
    {!! Form::file('image') !!}
</div> -->
<!-- <div class="form-group col-sm-12">
    <div class="fileinput fileinput-<?= $cls ?>" data-provides="fileinput">

        <div class="fileinput-new thumbnail">
        </div>

        <div class="fileinput-preview fileinput-exists thumbnail">
            @if(!empty($jenisProdukKatalog))
            <img src="{!! URL::to('uploads/jenis_produk/'.$jenisProdukKatalog->image) !!}">
            <input type="text" hidden name="image" value="{{ $jenisProdukKatalog->image }}">
            @endif
        </div>
        <div>
            <span class="btn btn-primary btn-file">
                <span class="fileinput-new">Select image</span>
                <span class="fileinput-exists">Change</span>
                <input type="file" name="image">
            </span>
            <a href="#" class="btn btn-primary fileinput-exists" data-dismiss="fileinput">Remove</a>
        </div>
    </div>
</div> -->

<!-- Submit Field -->
<div class="form-group col-sm-12 text-center">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('admin.careers.index') !!}" class="btn btn-default">Cancel</a>
</div>
