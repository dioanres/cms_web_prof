<?php
$cls = '';

if(empty($catalogPilarUsaha)) {
    $cls = 'new';
} else {
    if(!empty($catalogPilarUsaha->image)) {
        $cls = 'exists';
    } else {
        $cls = 'new';
    }
}

?>
<!-- Id Pilar Usaha Field -->
<div class="form-group col-sm-12">
    {!! Form::label('id_pilar_usaha', 'Pilar Usaha:') !!}
    {!! Form::select('id_pilar_usaha', $pilarUsahas, null, ['class' => 'form-control']) !!}
</div>

<!-- Image Field -->
<div class="form-group col-sm-12">
    {!! Form::label('image', 'Image:') !!}
    {!! Form::file('image') !!}
</div>
<div class="form-group col-sm-12">
    <div class="fileinput fileinput-<?= $cls ?>" data-provides="fileinput">

        <div class="fileinput-new thumbnail">
        </div>

        <div class="fileinput-preview fileinput-exists thumbnail">
            @if(!empty($catalogPilarUsaha))
            <img src="{!! URL::to('uploads/catalog/'.$catalogPilarUsaha->image) !!}">
            <input type="text" hidden name="image" value="{{ $catalogPilarUsaha->image }}">
            @endif
        </div>
        <div>
            <span class="btn btn-primary btn-file">
                <span class="fileinput-new">Select image</span>
                <span class="fileinput-exists">Change</span>
                <input type="file" name="image">
            </span>
            <a href="#" class="btn btn-primary fileinput-exists" data-dismiss="fileinput">Remove</a>
        </div>
    </div>
</div>
<div class="clearfix"></div>

<!-- Harga Field -->
<div class="form-group col-sm-12">
    {!! Form::label('harga', 'Harga:') !!}
    {!! Form::text('harga', null, ['class' => 'form-control rupiah']) !!}
</div>

<!-- Deskripsi Field -->
<div class="form-group col-sm-12">
    {!! Form::label('deskripsi', 'Deskripsi:') !!}
    {!! Form::text('deskripsi', null, ['class' => 'form-control']) !!}
</div>

<!-- Jenis Produk Field -->
<div class="form-group col-sm-12">
    {!! Form::label('jenis_produk', 'Jenis Produk:') !!}
    {!! Form::select('jenis_produk', $jenisProduk, null, ['class' => 'form-control']) !!}
</div>


<!-- Submit Field -->
<div class="form-group col-sm-12 text-center">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('admin.catalogPilarUsahas.index') !!}" class="btn btn-default">Cancel</a>
</div>
