<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $catalogPilarUsaha->id !!}</p>
    <hr>
</div>

<!-- Id Pilar Usaha Field -->
<div class="form-group">
    {!! Form::label('id_pilar_usaha', 'Id Pilar Usaha:') !!}
    <p>{!! $catalogPilarUsaha->id_pilar_usaha !!}</p>
    <hr>
</div>

<!-- Image Field -->
<div class="form-group">
    {!! Form::label('image', 'Image:') !!}
    <p>{!! $catalogPilarUsaha->image !!}</p>
    <hr>
</div>

<!-- Harga Field -->
<div class="form-group">
    {!! Form::label('harga', 'Harga:') !!}
    <p>{!! $catalogPilarUsaha->harga !!}</p>
    <hr>
</div>

<!-- Deskripsi Field -->
<div class="form-group">
    {!! Form::label('deskripsi', 'Deskripsi:') !!}
    <p>{!! $catalogPilarUsaha->deskripsi !!}</p>
    <hr>
</div>

<!-- Jenis Produk Field -->
<div class="form-group">
    {!! Form::label('jenis_produk', 'Jenis Produk:') !!}
    <p>{!! $catalogPilarUsaha->jenis_produk !!}</p>
    <hr>
</div>

