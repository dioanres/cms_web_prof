@extends('admin/layouts/default')

@section('title')
CatalogPilarUsahas
@parent
@stop

@section('content')
  @include('core-templates::common.errors')
    <section class="content-header">
     <h1>CatalogPilarUsahas Edit</h1>
     <ol class="breadcrumb">
         <li>
             <a href="{{ route('admin.dashboard') }}"> <i class="material-icons text-primary leftsize">home</i>
                 Dashboard
             </a>
         </li>
         <li>CatalogPilarUsahas</li>
         <li class="active">Edit CatalogPilarUsahas </li>
     </ol>
    </section>
    <section class="content paddingleft_right15">
      <div class="row">
      <div class="panel panel-primary">
            <div class="panel-heading">
                <h4 class="panel-title"> <i class="material-icons text-primary leftsize">edit</i>
                    Edit  CatalogPilarUsaha
                </h4></div>
            <br />
        <div class="panel-body">
        {!! Form::model($catalogPilarUsaha, ['route' => ['admin.catalogPilarUsahas.update', collect($catalogPilarUsaha)->first() ],'files' => true ,'method' => 'patch']) !!}

        @include('admin.catalogPilarUsahas.fields')

        {!! Form::close() !!}
        </div>
      </div>
    </div>
   </section>
 @stop
