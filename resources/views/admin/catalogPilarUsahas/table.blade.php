<style>
    .img-tumbnail {
        padding: 4px;
        line-height: 1.42857;
        background-color: #fff;
        border: 1px solid #ddd;
        border-radius: 3px;
        -webkit-transition: all .2s ease-in-out;
        -o-transition: all .2s ease-in-out;
        transition: all .2s ease-in-out;
        display: inline-block;
        max-width: 100%;
        height: auto;
    }
</style>
<table class="table table-responsive table-striped table-bordered" id="catalogPilarUsahas-table" width="100%">
    <thead>
     <tr>
        <th>Id Pilar Usaha</th>
        <th><center>Image</center></th>
        <th>Harga</th>
        <th>Deskripsi</th>
        <th>Jenis Produk</th>
        <th >Action</th>
     </tr>
    </thead>
    <tbody>
    @foreach($catalogPilarUsahas as $catalogPilarUsaha)
        <tr>
            <td>{!! \App\Models\PilarUsaha::select('nama_pilar_usaha')->where('id',$catalogPilarUsaha->id_pilar_usaha)->first()->nama_pilar_usaha !!}</td>
            <td>
                <center>
                    @if($catalogPilarUsaha->image != null)
                        <img src="{!! URL::to('uploads/catalog/'.$catalogPilarUsaha->image) !!}" width="80" height="80" class="img-tumbnail">
                    @else
                        <img src="{!! URL::to('assets/images/no-image.png') !!}" width="80" height="80" class="img-tumbnail">
                    @endif
                </center>
            </td>
            <td>{!! number_format($catalogPilarUsaha->harga) !!}</td>
            <td>{!! $catalogPilarUsaha->deskripsi !!}</td>
            <td>{!! \App\Models\JenisProdukKatalog::where('id',$catalogPilarUsaha->jenis_produk)->first()->nama_jenis_produk  !!}</td>
            <td>
                 <!-- <a href="{{ route('admin.catalogPilarUsahas.show', collect($catalogPilarUsaha)->first() ) }}">
                     <i class="material-icons text-primary leftsize" title="view catalogPilarUsaha">info</i>
                 </a>
                 -->
                 <a href="{{ route('admin.catalogPilarUsahas.edit', collect($catalogPilarUsaha)->first() ) }}">
                     <i class="material-icons text-warning leftsize" title="edit catalogPilarUsaha">edit</i> 
                 </a>
                 <a href="{{ route('admin.catalogPilarUsahas.confirm-delete', collect($catalogPilarUsaha)->first() ) }}" data-toggle="modal" data-target="#delete_confirm">
                     <i class="material-icons text-danger leftsize" title="delete catalogPilarUsaha">delete</i>
                 </a>
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
@section('footer_scripts')
    <div class="modal fade" id="delete_confirm" tabindex="-1" role="dialog" aria-labelledby="user_delete_confirm_title" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
            </div>
        </div>
    </div>
    <script>$(function () {$('body').on('hidden.bs.modal', '.modal', function () {$(this).removeData('bs.modal');});});</script>
<link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/buttons.bootstrap.css') }}"/>
<link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/dataTables.bootstrap.css') }}"/>
 <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/buttons.bootstrap.css') }}">
<script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/jquery.dataTables.js') }}" ></script>
 <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.bootstrap.js') }}" ></script>

    <script>
        $('#catalogPilarUsahas-table').DataTable({
                      responsive: true,
                      pageLength: 10
        });
         $('#catalogPilarUsahas-table').on( 'length.dt', function ( e, settings, len ) {
                     setTimeout(function(){
                         $('.livicon').updateLivicon();
                     },200);
         });

       </script>
@stop
