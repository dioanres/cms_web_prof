<div class="form-group">
    <label>Menu</label>
    <select class="form-control" name="menu_utama" disabled>
        <option>Pilih Menu Utama</option>
        @foreach ($menu_utama as $menu)
        <option value="{{ $menu->id }}" <?= $content == null ? '' : $content->menu_utama == $menu->id ? 'selected' : '' ?> >{{ $menu->nama_menu }}</option>
        @endforeach
    </select>
</div>

<!-- Title Field -->
<div class="form-group">
    {!! Form::label('title', 'Title:') !!}
    <p>{!! $content->title !!}</p>
    <hr>
</div>

@if(Session::get('menu_id') != config('menu.clients'))
<!-- Content Field -->
<div class="form-group">
    {!! Form::label('content', 'Content:') !!}
    <p>{!! $content->content !!}</p>
    <hr>
</div>

<!-- Alamat -->
<div class="form-group">
    {!! Form::label('alamat', 'Alamat:') !!}
    <p>{!! $content->alamat !!}</p>
    <hr>
</div>

<!-- Value PAB -->
<div class="form-group">
    {!! Form::label('value', 'Value PAB:') !!}
    <p>{!! $content->value !!}</p>
    <hr>
</div>

<!-- No HP -->
<div class="form-group">
    {!! Form::label('no_hp', 'No HP:') !!}
    <p>{!! $content->no_hp !!}</p>
    <hr>
</div>

<!-- No Telp -->
<div class="form-group">
    {!! Form::label('no_telp', 'No Telp:') !!}
    <p>{!! $content->no_telp !!}</p>
    <hr>
</div>

<!-- Email -->
<div class="form-group">
    {!! Form::label('email', 'Email:') !!}
    <p>{!! $content->email !!}</p>
    <hr>
</div>

<!-- IG -->
<div class="form-group">
    {!! Form::label('ig', 'Instagram:') !!}
    <p>@ {!! $content->ig !!}</p>
    <hr>
</div>

<!-- Twitter -->
<div class="form-group">
    {!! Form::label('twitter', 'Twitter:') !!}
    <p>@ {!! $content->twitter !!}</p>
    <hr>
</div>

<!-- FB -->
<div class="form-group">
    {!! Form::label('facebook', 'Facebook:') !!}
    <p>@ {!! $content->facebook !!}</p>
    <hr>
</div>

<!-- Visi -->
<div class="form-group">
    {!! Form::label('visi', 'Visi:') !!}
    <p>{!! $content->visi !!}</p>
    <hr>
</div>

<!-- Misi -->
<div class="form-group">
    {!! Form::label('misi', 'Misi:') !!}
    <p>{!! $content->misi !!}</p>
    <hr>
</div>

<!-- Url Video Field -->
<div class="form-group">
    {!! Form::label('url_video', 'Url Video:') !!}
    <p>{!! $content->url_video !!}</p>
    <hr>
</div>

@endif
<!-- Image Field -->
<div class="form-group">
    {!! Form::label('image', 'Image:') !!}
    <br/>
    <img src="{!! URL::to('uploads/content/'.$content->image) !!}" height="150px" width="200px">
    <div>
</div>



