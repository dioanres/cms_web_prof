@extends('admin/layouts/default')

@section('title')
Contents
@parent
@stop

@section('content')
@include('core-templates::common.errors')
<section class="content-header">
    <h1>Contents Edit</h1>
    <ol class="breadcrumb">
        <li>
            <a href="{{ route('admin.dashboard') }}"> <i class="material-icons text-primary leftsize">home</i>
                Dashboard
            </a>
        </li>
        <li>Contents</li>
        <li class="active">Edit Contents </li>
    </ol>
</section>
<section class="content paddingleft_right15">
    <div class="row">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <h4 class="panel-title"> <i class="material-icons text-primary leftsize">edit</i>
                    Edit Content
                </h4>
            </div>
            <br />
            <div class="panel-body">
                {!! Form::model($content, ['route' => ['admin.contents.update', collect($content)->first() ], 'method'
                => 'patch','files' =>true]) !!}

                @include('admin.contents.fields')

                {!! Form::close() !!}
            </div>
        </div>
    </div>
</section>
@stop

@section('footer_scripts')
<!-- Bootstrap WYSIHTML5 -->
<script>
$('.tambah-misi').click(function() {

    var _clone = '<div class="col-sm-12 misi-field">' +
        '        <div class="col-sm-10">' +
        '            <input type="text" class="form-control col-sm-6" name="misi[]" placeholder="Misi Perusahaan">' +
        '        </div>' +
        '        <div class="col-sm-2">' +
        '            <button class="btn btn-danger hapus-misi" type="button"><i class=". fa-2x fas fa-minus-circle"></i></button>' +
        '        </div>' +
        '    </div>';
    $('.clone').append(_clone);
});

$(document).on("click", ".hapus-misi", function() {
    $(this).parent().parent().remove();
})
</script>
@stop