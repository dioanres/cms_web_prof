@extends('admin/layouts/default')

@section('title')
Contents
@parent
@stop
@section('header_styles')


@stop
@section('content')
@include('core-templates::common.errors')
<section class="content-header">
    <h1>Contents</h1>
    <ol class="breadcrumb">
        <li>
            <a href="{{ route('admin.dashboard') }}"> <i class="material-icons text-primary leftsize">home</i>
                Dashboard
            </a>
        </li>
        <li>Contents</li>
        <li class="active">Create Contents </li>
    </ol>
</section>
<section class="content paddingleft_right15">
    <div class="row">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <h4 class="panel-title"> <i class="material-icons text-primary leftsize">add_circle</i>
                    Create New Content
                </h4>
            </div>
            <br />
            <div class="panel-body">
                {!! Form::open(['route' => 'admin.contents.store','files'=>true]) !!}

                @include('admin.contents.fields')

                {!! Form::close() !!}
            </div>
        </div>
    </div>
</section>
@stop

@section('footer_scripts')
<!-- Bootstrap WYSIHTML5 -->

@stop
