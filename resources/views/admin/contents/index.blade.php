@extends('admin/layouts/default')

@section('title')
Contents
@parent
@stop

{{-- Page content --}}
@section('content')
<section class="content-header">
    <h1>Contents</h1>
    <ol class="breadcrumb">
        <li>
            <a href="{{ route('admin.dashboard') }}"> <i class="material-icons text-primary leftsize">home</i>
                Dashboard
            </a>
        </li>
        <li>Contents</li>
        <li class="active">Contents List</li>
    </ol>
</section>

<section class="content paddingleft_right15">
    <div class="row">
     @include('flash::message')
        <div class="panel panel-primary ">
            <div class="panel-heading clearfix">
                <h4 class="panel-title pull-left"> <i class="material-icons">list</i>
                    Contents List
                </h4>
                @if (Session::get('menu_id') != config('menu.about_us'))
                <div class="pull-right">
                    <a href="{{ route('admin.contents.create') }}" class="btn btn-sm btn-default"><span class="glyphicon glyphicon-plus"></span> @lang('button.create')</a>
                </div>
                @endif
            </div>
            <br />
            <div class="panel-body table-responsive">
                 @include('admin.contents.table')
                 
            </div>
        </div>
 </div>
</section>
@stop
