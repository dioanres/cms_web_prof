<table class="table table-responsive table-striped table-bordered" id="contents-table" width="100%">
    <thead>
        <tr>
            <th>Menu Utama</th>
            @if(Session::get('menu_id') == config('menu.clients'))
            <th>Nama Clients</th>
            <th>Image</th>
            @else
            <th>Title</th>
            <th>Content</th>
            <!-- <th>Logo</th> -->
            <th>Url Video</th>
            @endif
            <th>Action</th>
        </tr>
    </thead>
    <tbody>
        @foreach($contents as $content)
        <tr>
            <td>{!! App\Models\MenuUtama::find($content->menu_utama)->nama_menu !!}</td>
            
            <td>{!! $content->title !!}</td>
            @if(Session::get('menu_id') == config('menu.clients'))
            <td><img src="{!! URL::to('uploads/content/'.$content->image) !!}" width="200px" height="150px"></td>
            @else
            <td>{!! $content->content !!}</td>
            <!-- <td><img src="{!! URL::to('uploads/content/'.$content->image) !!}" width="200px" height="150px"></td> -->
            <td>{!! $content->url_video !!}</td>
            @endif
            
            <td>
                <!-- <a href="{{ route('admin.contents.show', collect($content)->first() ) }}">
                    <i class="material-icons text-primary leftsize" title="view content">info</i>
                </a> -->
                <a href="{{ route('admin.contents.edit', collect($content)->first() ) }}">
                <button type="button" class="btn btn-warning btn-sm">Edit</button>
                </a>
                @if (Session::get('menu_id') != config('menu.about_us'))
                <a href="{{ route('admin.contents.confirm-delete', collect($content)->first() ) }}" data-toggle="modal"
                    data-target="#delete_confirm">
                    <i class="material-icons text-danger leftsize" title="delete content">delete</i>
                </a>
                @endif
            </td>
        </tr>
        @endforeach
    </tbody>
</table>
@section('footer_scripts')
<div class="modal fade" id="delete_confirm" tabindex="-1" role="dialog" aria-labelledby="user_delete_confirm_title"
    aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
        </div>
    </div>
</div>
<script>
$(function() {
    $('body').on('hidden.bs.modal', '.modal', function() {
        $(this).removeData('bs.modal');
    });
});
</script>
<link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/buttons.bootstrap.css') }}" />
<link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/dataTables.bootstrap.css') }}" />
<link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/buttons.bootstrap.css') }}">
<script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/jquery.dataTables.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.bootstrap.js') }}"></script>

<script>
$('#contents-table').DataTable({
    responsive: true,
    pageLength: 10
});
$('#contents-table').on('length.dt', function(e, settings, len) {
    setTimeout(function() {
        $('.livicon').updateLivicon();
    }, 200);
});
</script>
@stop