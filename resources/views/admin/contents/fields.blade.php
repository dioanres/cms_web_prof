<?php
$cls = '';
$cls_icon_hp = '';
$cls_icon_office= '';
$cls_icon_form= '';

if(empty($content)) {
    $cls = 'new';
} else {
    if(!empty($content->image)) {
        $cls = 'exists';
    } else {
        $cls = 'new';
    }
}

if(empty($content)) {
    $cls_icon_hp = 'new';
} else {
    if(!empty($content->icon_hp)) {
        $cls_icon_hp = 'exists';
    } else {
        $cls_icon_hp = 'new';
    }
}

if(empty($content)) {
    $cls_icon_office = 'new';
} else {
    if(!empty($content->icon_office)) {
        $cls_icon_office = 'exists';
    } else {
        $cls_icon_office = 'new';
    }
}

if(empty($content)) {
    $cls_icon_form = 'new';
} else {
    if(!empty($content->icon_form)) {
        $cls_icon_form = 'exists';
    } else {
        $cls_icon_form = 'new';
    }
}

?>

<style>
.thumbnail {
    width:120px;
    height:120px;
}
</style>
<div class="form-group col-sm-12">
    <label>Menu</label>
    <select class="form-control" name="menu_utama" disabled>
        <option>Pilih Menu Utama</option>
        @foreach ($menu_utama as $menu)
        <option value="{{ $menu->id }}" <?= Session::get('menu_id') == $menu->id ? 'selected' : '' ?>>
            {{ $menu->nama_menu }}</option>
        @endforeach
    </select>
</div>
<!-- Title Field -->
<div class="form-group col-sm-12">
    @if(Session::get('menu_id') == config('menu.clients'))
    {!! Form::label('title', 'Nama Client:') !!}
    @else
    {!! Form::label('title', 'Title:') !!}
    @endif

    {!! Form::text('title', null, ['class' => 'form-control']) !!}
</div>

@if(Session::get('menu_id') != config('menu.clients'))
<!-- Content Bold -->
<div class="form-group col-sm-12">
    <div class="panel panel-success">
        <div class="panel-heading">
            <div class="text-muted bootstrap-admin-box-title editor-clr">
                <h3 class="panel-title">
                    Content Bold</h3>
            </div>
        </div>
        <div class="bootstrap-admin-panel-content">
            <textarea id="content_bold" class="ckeditor_full" name="content_bold">{{ $content != null ? $content->content_bold : '' }}</textarea>
        </div>
    </div>
</div>

<!-- Content Field -->
<div class="form-group col-sm-12">
    <div class="panel panel-success">
        <div class="panel-heading">
            <div class="text-muted bootstrap-admin-box-title editor-clr">
                <h3 class="panel-title">
                    Content</h3>
            </div>
        </div>
        <div class="bootstrap-admin-panel-content">
            <textarea id="ckeditor_full1" class="ckeditor_full" name="content">{{ $content != null ? $content->content : '' }}</textarea>
        </div>
    </div>
</div>
<!-- Alamat -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('alamat', 'Address:') !!}
    {!! Form::textarea('alamat', null, ['class' => 'form-control', 'rows' => '5']) !!}
</div>

<div class="form-group col-sm-12">
    {!! Form::label('citizen_country', 'City - Country:') !!}
    {!! Form::text('citizen_country', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group col-sm-12">
    <div class="form-group col-sm-2">
        {!! Form::label('Icon Office', 'Icon Office Information:') !!}
        <br />
        <div class="fileinput fileinput-<?= $cls_icon_office ?>" data-provides="fileinput">
            <div class="fileinput-new thumbnail">
            </div>
            <div class="fileinput-preview fileinput-exists thumbnail">
                @if(!empty($content))
                <img src="{!! URL::to('uploads/content/'.$content->icon_office) !!}">
                <input type="text" hidden name="icon_office" value="{{ $content->icon_office }}">
                @endif
            </div>
            <div>
                <span class="btn btn-primary btn-file">
                    <span class="fileinput-new">Select image</span>
                    <span class="fileinput-exists">Change</span>
                    <input type="file" name="icon_office">
                </span>
                <a href="#" class="btn btn-primary fileinput-exists" data-dismiss="fileinput">Remove</a>
            </div>
        </div>
    </div>

    <div class="form-group col-sm-10">
        {!! Form::label('citizen_information', 'Office Information:') !!}
        {!! Form::text('citizen_information', null, ['class' => 'form-control']) !!}
    </div>
    
</div>

<!-- <div class="form-group col-sm-12">
    
</div> -->
<!-- Value PAB -->
<!-- <div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('value', 'Value:') !!}
    {!! Form::textarea('value', null, ['class' => 'form-control', 'rows' => '5']) !!}
</div> -->

<!-- No HP -->
<div class="form-group col-sm-12">
    <div class="form-group col-sm-2">
        {!! Form::label('Icon No HP', 'Icon No HP:') !!}
        <br />
        <div class="fileinput fileinput-<?= $cls_icon_hp ?>" data-provides="fileinput">

            <div class="fileinput-new thumbnail">
            </div>

            <div class="fileinput-preview fileinput-exists thumbnail">
                @if(!empty($content))
                <img src="{!! URL::to('uploads/content/'.$content->icon_hp) !!}">
                <input type="text" hidden name="icon_hp" value="{{ $content->icon_hp }}">
                @endif
            </div>
            <div>
                <span class="btn btn-primary btn-file">
                    <span class="fileinput-new">Select image</span>
                    <span class="fileinput-exists">Change</span>
                    <input type="file" name="icon_hp">
                </span>
                <a href="#" class="btn btn-primary fileinput-exists" data-dismiss="fileinput">Remove</a>
            </div>
        </div>
    </div>

    <div class="form-group col-sm-10">
        {!! Form::label('no_hp', 'Hp No.:') !!}
        {!! Form::text('no_hp', null, ['class' => 'form-control']) !!}
    </div>
</div>


<div class="form-group col-sm-12">
    <div class="form-group col-sm-2">
        {!! Form::label('Icon Form Email', 'Icon Form Email:') !!}
        <!-- <span>type image .png (20x20)</span> -->
        <br />
        <div class="fileinput fileinput-<?= $cls_icon_form ?>" data-provides="fileinput">
            <div class="fileinput-new thumbnail">
            </div>
            <div class="fileinput-preview fileinput-exists thumbnail">
                @if(!empty($content))
                <img src="{!! URL::to('uploads/content/'.$content->icon_form) !!}">
                <input type="text" hidden name="icon_form" value="{{ $content->icon_form }}">
                @endif
            </div>
            <div>
                <span class="btn btn-primary btn-file">
                    <span class="fileinput-new">Select image</span>
                    <span class="fileinput-exists">Change</span>
                    <input type="file" name="icon_form">
                </span>
                <a href="#" class="btn btn-primary fileinput-exists" data-dismiss="fileinput">Remove</a>
            </div>
        </div>
    </div>

    <div class="form-group col-sm-10">
        {!! Form::label('email', 'Email:') !!}
        {!! Form::text('email', null, ['class' => 'form-control']) !!}
    </div>
    
</div>


<!-- No Telp -->
<div class="form-group col-sm-12">
    {!! Form::label('no_telp', 'Phone No.:') !!}
    {!! Form::text('no_telp', null, ['class' => 'form-control']) !!}
</div>

<!-- Ig Field -->
<div class="form-group input-group col-sm-12">
    <span class="input-group-addon">@</span>
    <input type="text" class="form-control" placeholder="Instagram" name="ig">
</div>

<!-- Twitter Field -->
<div class="form-group input-group col-sm-12">
    <span class="input-group-addon">@</span>
    <input type="text" class="form-control" placeholder="Twitter" name="twitter">
</div>

<!-- Fb Field -->
<div class="form-group input-group col-sm-12">
    <span class="input-group-addon">@</span>
    <input type="text" class="form-control" placeholder="Facebook" name="facebook">
</div>

<!-- Visi -->
<!-- <div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('visi', 'Visi Perusahaan:') !!}
    {!! Form::textarea('visi', null, ['class' => 'form-control', 'rows' => '5']) !!}
</div> -->

<!-- <div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('misi', 'Misi Perusahaan:') !!}
    <div class="clone">
        @if(!empty($misi))
        @foreach ($misi as $v)
        <div class="col-sm-12">
            <div class="col-sm-10">
                <input type="text" class="form-control col-sm-6" name="misi[]" value="{{ $v->misi }}"
                    placeholder="Misi Perusahaan">
            </div>
            @if(!$loop->first)
            <div class="col-sm-2">
            <button class="btn btn-danger hapus-misi" type="button"><i class=". fa-2x fas fa-minus-circle"></i></button>
        </div>
            @endif
        </div>
        @endforeach

        @else
        <div class="col-sm-12">
            <div class="col-sm-10">
                <input type="text" class="form-control col-sm-6" name="misi[]" placeholder="Misi Perusahaan">
            </div>
        </div>
        @endif


    </div>
    <button class="btn btn-success tambah-misi" type="button"><i class=". fa-2x fas fa-plus-circle"></i></button>
</div> -->

<!-- Misi -->
<!-- <div class="form-group col-sm-12">
    <div class="panel panel-success">
        <div class="panel-heading">
            <div class="text-muted bootstrap-admin-box-title editor-clr">
                <h3 class="panel-title">
                    Misi Perusahaan</h3>
            </div>
        </div>
        <div class="bootstrap-admin-panel-content">
            <textarea class="ckeditor_full" name="misi">{{ $content != null ? $content->misi : '' }}</textarea>
        </div>
    </div>
</div> -->
@endif

<!-- Image Field -->
@if(Session::get('menu_id') != config('menu.value_pab'))
<!-- <div class="form-group col-sm-12">
    <div class="fileinput fileinput-<?= $cls ?>" data-provides="fileinput">

        <div class="fileinput-new thumbnail">
        </div>

        <div class="fileinput-preview fileinput-exists thumbnail">
            @if(!empty($content))
            <img src="{!! URL::to('uploads/content/'.$content->image) !!}">
            <input type="text" hidden name="image" value="{{ $content->image }}">
            @endif
        </div>
        <div>
            <span class="btn btn-primary btn-file">
                <span class="fileinput-new">Select image</span>
                <span class="fileinput-exists">Change</span>
                <input type="file" name="image">
            </span>
            <a href="#" class="btn btn-primary fileinput-exists" data-dismiss="fileinput">Remove</a>
        </div>
    </div>
</div> -->

<div class="clearfix"></div>

@if(Session::get('menu_id') != config('menu.clients'))
<!-- Url Video Field -->
<div class="form-group col-sm-12">
    {!! Form::label('url_video', 'Url Video:') !!}
    {!! Form::text('url_video', null, ['class' => 'form-control']) !!}
</div>
@endif
@endif
<!-- Submit Field -->
<div class="form-group col-sm-12 text-center">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{{ URL::to('admin/'.Session::get('menu_id').'/contents') }}" class="btn btn-default">Cancel</a>
</div>