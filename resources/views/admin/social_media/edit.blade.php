@extends('admin/layouts/default')

@section('title')
Social Media
@parent
@stop

@section('content')
  @include('core-templates::common.errors')
    <section class="content-header">
     <h1>Social Media Edit</h1>
     <ol class="breadcrumb">
         <li>
             <a href="{{ route('admin.dashboard') }}"> <i class="material-icons text-primary leftsize">home</i>
                 Dashboard
             </a>
         </li>
         <li>Social Media</li>
         <li class="active">Edit Social Media </li>
     </ol>
    </section>
    <section class="content paddingleft_right15">
      <div class="row">
      <div class="panel panel-primary">
            <div class="panel-heading">
                <h4 class="panel-title"> <i class="material-icons text-primary leftsize">edit</i>
                    Edit  Social Media
                </h4></div>
            <br />
        <div class="panel-body">
        {!! Form::model($social_media, ['route' => ['admin.social_media.update', collect($social_media)->first() ], 'method' => 'patch','files' => true]) !!}

        @include('admin.social_media.fields')

        {!! Form::close() !!}
        </div>
      </div>
    </div>
   </section>
 @stop
