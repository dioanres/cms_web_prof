<?php

$cls= '';

if(empty($social_media)) {
    $cls = 'new';
} else {
    if(!empty($social_media->icon)) {
        $cls = 'exists';
    } else {
        $cls = 'new';
    }
}

?>
<style>
.thumbnail {
    width:80px;
    height:80px;
}
</style>
<!-- icon -->
<div class="form-group col-sm-2">
    {!! Form::label('Icon', 'Icon:') !!}
    <!-- <span>type image .png (20x20)</span> -->
    <br />
    <div class="fileinput fileinput-<?= $cls ?>" data-provides="fileinput">
        <div class="fileinput-new thumbnail">
        </div>
        <div class="fileinput-preview fileinput-exists thumbnail">
            @if(!empty($social_media))
            <img src="{!! URL::to('uploads/social_media/'.$social_media->icon) !!}">
            <input type="text" hidden name="icon" value="{{ $social_media->icon }}">
            @endif
        </div>
        <div>
            <span class="btn btn-primary btn-file">
                <span class="fileinput-new">Select image</span>
                <span class="fileinput-exists">Change</span>
                <input type="file" name="icon">
            </span>
            <a href="#" class="btn btn-primary fileinput-exists" data-dismiss="fileinput">Remove</a>
        </div>
    </div>
</div>
<!-- Nama Jenis Produk Field -->
<div class="form-group col-sm-12">
    {!! Form::label('name', 'Name:') !!}
    {!! Form::text('name', null, ['class' => 'form-control']) !!}
</div>

<!-- Nama Jenis Produk Field -->
<div class="form-group col-sm-12">
    {!! Form::label('url', 'URL:') !!}
    {!! Form::text('url', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12 text-center">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('admin.social_media.index') !!}" class="btn btn-default">Cancel</a>
</div>
