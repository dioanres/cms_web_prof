<table class="table table-responsive table-striped table-bordered" id="karyawans-table" width="100%">
    <thead>
     <tr>
        <th>Nama</th>
        <th>Jabatan</th>
        <th>Catatan</th>
        <th>Foto</th>
        <th>Ig</th>
        <th>Twitter</th>
        <th>Fb</th>
        <th >Action</th>
     </tr>
    </thead>
    <tbody>
    @foreach($karyawans as $karyawan)
    
        <tr>
            <td>{!! $karyawan->nama !!}</td>
            <td>{!! $karyawan->jabatan !!}</td>
            <td>{!! $karyawan->catatan !!}</td>
            <td>
                @if ($karyawan->foto != null)
                <img src="{!! URL::to('uploads/karyawan/'.$karyawan->foto) !!}" width="200px" height="150px">
                @else
                <img src="{!! URL::to('assets/images/authors/no_avatar.jpg') !!}" width="200px" height="150px">
                @endif
            </td>
            <td><a href="https://www.instagram.com/{!! $karyawan->ig !!}">{!! $karyawan->ig !!}</a></td>
            <td><a href="https://www.twitter.com/{!! $karyawan->twitter !!}">{!! $karyawan->twitter !!}</a> </td>
            <td>{!! $karyawan->fb !!}</td>
            <td>
                 <!-- <a href="{{ route('admin.karyawans.show', collect($karyawan)->first() ) }}">
                     <i class="material-icons text-primary leftsize" title="view karyawan">info</i>
                 </a> -->
                 <a href="{{ route('admin.karyawans.edit', collect($karyawan)->first() ) }}">
                     <i class="material-icons text-warning leftsize" title="edit karyawan">edit</i>
                 </a>
                 <a href="{{ route('admin.karyawans.confirm-delete', collect($karyawan)->first() ) }}" data-toggle="modal" data-target="#delete_confirm">
                     <i class="material-icons text-danger leftsize" title="delete karyawan">delete</i>
                 </a>
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
@section('footer_scripts')
    <div class="modal fade" id="delete_confirm" tabindex="-1" role="dialog" aria-labelledby="user_delete_confirm_title" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
            </div>
        </div>
    </div>
    <script>$(function () {$('body').on('hidden.bs.modal', '.modal', function () {$(this).removeData('bs.modal');});});</script>
<link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/buttons.bootstrap.css') }}"/>
<link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/dataTables.bootstrap.css') }}"/>
 <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/buttons.bootstrap.css') }}">
<script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/jquery.dataTables.js') }}" ></script>
 <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.bootstrap.js') }}" ></script>

    <script>
        $('#karyawans-table').DataTable({
                      responsive: true,
                      pageLength: 10
        });
         $('#karyawans-table').on( 'length.dt', function ( e, settings, len ) {
                     setTimeout(function(){
                         $('.livicon').updateLivicon();
                     },200);
         });

       </script>
@stop
