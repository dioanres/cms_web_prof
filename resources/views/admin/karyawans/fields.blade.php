<?php
$cls = '';

if(empty($karyawan)) {
    $cls = 'new';
} else {
    if(!empty($karyawan->image)) {
        $cls = 'exists';
    } else {
        $cls = 'new';
    }
}

?>
<!-- Nama Field -->
<div class="form-group col-sm-12">
    {!! Form::label('nama', 'Nama:') !!}
    {!! Form::text('nama', null, ['class' => 'form-control']) !!}
</div>

<!-- Jabatan Field -->
<div class="form-group col-sm-12">
    {!! Form::label('jabatan', 'Jabatan:') !!}
    {!! Form::text('jabatan', null, ['class' => 'form-control']) !!}
</div>

<!-- Catatan Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('catatan', 'Catatan:') !!}
    {!! Form::textarea('catatan', null, ['class' => 'form-control', 'rows' => '5']) !!}
</div>


<!-- Foto Field -->
<div class="form-group col-sm-12">
    <div class="fileinput fileinput-<?= $cls ?>" data-provides="fileinput">

        <div class="fileinput-new thumbnail">
        </div>

        <div class="fileinput-preview fileinput-exists thumbnail">
            @if(!empty($karyawan))
            <img src="{!! URL::to('uploads/karyawan/'.$karyawan->foto) !!}">
            <input type="text" hidden name="foto" value="{{ $karyawan->foto }}">
            @endif
        </div>
        <div>
            <span>Ukuran Foto: 170 x 170</span>
            <br/>
            <span class="btn btn-primary btn-file">
                <span class="fileinput-new">Select image</span>
                <span class="fileinput-exists">Change</span>
                <input type="file" name="foto">
            </span>
            <a href="#" class="btn btn-primary fileinput-exists" data-dismiss="fileinput">Remove</a>
        </div>
    </div>
</div>
<!-- <div class="form-group col-sm-12">
    {!! Form::label('foto', 'Foto:') !!}
    {!! Form::file('foto') !!}
</div> -->
<div class="clearfix"></div>

<!-- Ig Field -->
<div class="form-group input-group col-sm-12">
    <span class="input-group-addon"><img height="30px" src="{!! URL::to('assets/img/icon/ig.png') !!}"></span>
    <input type="text" class="form-control" placeholder="Instagram" name="ig" value="<?= @$karyawan->ig ?>">
</div>

<!-- Twitter Field -->
<div class="form-group input-group col-sm-12">
    <span class="input-group-addon"><img height="30px" src="{!! URL::to('assets/img/icon/twitter.png') !!}"></span>
    <input type="text" class="form-control" placeholder="Twitter" name="twitter" value="<?= @$karyawan->twitter ?>">
</div>

<!-- Fb Field -->
<!-- <div class="form-group input-group col-sm-12">
    <span class="input-group-addon"></span>
    <input type="text" class="form-control" placeholder="Facebook" name="fb" value="<?= @$karyawan->fb ?>">
</div> -->

<!-- Linkedin Field -->
<div class="form-group input-group col-sm-12">
    <span class="input-group-addon"><img height="30px" src="{!! URL::to('assets/img/icon/linkedin.png') !!}"></span>
    <input type="text" class="form-control" placeholder="Linkedin" name="linkedin" value="<?= @$karyawan->linkedin ?>">
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12 text-center">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('admin.karyawans.index') !!}" class="btn btn-default">Cancel</a>
</div>
