<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $karyawan->id !!}</p>
    <hr>
</div>

<!-- Nama Field -->
<div class="form-group">
    {!! Form::label('nama', 'Nama:') !!}
    <p>{!! $karyawan->nama !!}</p>
    <hr>
</div>

<!-- Jabatan Field -->
<div class="form-group">
    {!! Form::label('jabatan', 'Jabatan:') !!}
    <p>{!! $karyawan->jabatan !!}</p>
    <hr>
</div>

<!-- Catatan Field -->
<div class="form-group">
    {!! Form::label('catatan', 'Catatan:') !!}
    <p>{!! $karyawan->catatan !!}</p>
    <hr>
</div>

<!-- Foto Field -->
<div class="form-group">
    {!! Form::label('foto', 'Foto:') !!}
    <p>{!! $karyawan->foto !!}</p>
    <hr>
</div>

<!-- Ig Field -->
<div class="form-group">
    {!! Form::label('ig', 'Ig:') !!}
    <p>{!! $karyawan->ig !!}</p>
    <hr>
</div>

<!-- Twitter Field -->
<div class="form-group">
    {!! Form::label('twitter', 'Twitter:') !!}
    <p>{!! $karyawan->twitter !!}</p>
    <hr>
</div>

<!-- Fb Field -->
<div class="form-group">
    {!! Form::label('fb', 'Fb:') !!}
    <p>{!! $karyawan->fb !!}</p>
    <hr>
</div>

