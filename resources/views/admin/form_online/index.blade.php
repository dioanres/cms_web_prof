@extends('admin/layouts/default')

@section('title')
Form Online
@parent
@stop

{{-- Page content --}}
@section('content')
<section class="content-header">
    <h1>{{$title}}</h1>
    <ol class="breadcrumb">
        <li>
            <a href="{{ route('admin.dashboard') }}"> <i class="material-icons text-primary leftsize">home</i>
                Dashboard
            </a>
        </li>
        <li>Career</li>
        <li class="active">{{$title}} List</li>
    </ol>
</section>

<section class="content paddingleft_right15">
    <div class="row">
     @include('flash::message')
        <div class="panel panel-primary ">
            <div class="panel-heading clearfix">
                <h4 class="panel-title pull-left"> <i class="material-icons">list</i>
                {{$title}} List
                </h4>
                
            </div>
            <br />
            <div class="panel-body table-responsive">
                 @include('admin.form_online.table')
                 
            </div>
        </div>
 </div>
</section>
@stop
