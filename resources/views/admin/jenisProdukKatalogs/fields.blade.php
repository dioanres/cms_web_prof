<?php
$cls = '';

if(empty($jenisProdukKatalog)) {
    $cls = 'new';
} else {
    if(!empty($jenisProdukKatalog->image)) {
        $cls = 'exists';
    } else {
        $cls = 'new';
    }
}

?>
<!-- Nama Jenis Produk Field -->
<div class="form-group col-sm-12">
    {!! Form::label('nama_jenis_produk', 'Nama Jenis Produk:') !!}
    {!! Form::text('nama_jenis_produk', null, ['class' => 'form-control']) !!}
</div>

<!-- Image Field -->
<div class="form-group col-sm-12">
    {!! Form::label('image', 'Image:') !!}
    {!! Form::file('image') !!}
</div>
<div class="form-group col-sm-12">
    <div class="fileinput fileinput-<?= $cls ?>" data-provides="fileinput">

        <div class="fileinput-new thumbnail">
        </div>

        <div class="fileinput-preview fileinput-exists thumbnail">
            @if(!empty($jenisProdukKatalog))
            <img src="{!! URL::to('uploads/jenis_produk/'.$jenisProdukKatalog->image) !!}">
            <input type="text" hidden name="image" value="{{ $jenisProdukKatalog->image }}">
            @endif
        </div>
        <div>
            <span class="btn btn-primary btn-file">
                <span class="fileinput-new">Select image</span>
                <span class="fileinput-exists">Change</span>
                <input type="file" name="image">
            </span>
            <a href="#" class="btn btn-primary fileinput-exists" data-dismiss="fileinput">Remove</a>
        </div>
    </div>
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12 text-center">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('admin.jenisProdukKatalogs.index') !!}" class="btn btn-default">Cancel</a>
</div>
