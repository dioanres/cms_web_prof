@extends('admin/layouts/default')

@section('title')
JenisProdukKatalogs
@parent
@stop

@section('content')
@include('core-templates::common.errors')
<section class="content-header">
    <h1>JenisProdukKatalogs</h1>
    <ol class="breadcrumb">
        <li>
            <a href="{{ route('admin.dashboard') }}"> <i class="material-icons text-primary leftsize">home</i>
                Dashboard
            </a>
        </li>
        <li>Jenis Produk Katalog</li>
        <li class="active">Tambah Jenis Produk Katalog </li>
    </ol>
</section>
<section class="content paddingleft_right15">
<div class="row">
 <div class="panel panel-primary">
        <div class="panel-heading">
            <h4 class="panel-title"> <i class="material-icons text-primary leftsize">add_circle</i>
                New  Jenis Produk Katalog
            </h4></div>
        <br />
        <div class="panel-body">
        {!! Form::open(['route' => 'admin.jenisProdukKatalogs.store','files' => true]) !!}

            @include('admin.jenisProdukKatalogs.fields')

        {!! Form::close() !!}
    </div>
  </div>
 </div>
</section>
 @stop
