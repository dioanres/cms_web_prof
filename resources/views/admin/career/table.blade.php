<style>
    .img-tumbnail {
        padding: 4px;
        line-height: 1.42857;
        background-color: #fff;
        border: 1px solid #ddd;
        border-radius: 3px;
        -webkit-transition: all .2s ease-in-out;
        -o-transition: all .2s ease-in-out;
        transition: all .2s ease-in-out;
        display: inline-block;
        max-width: 100%;
        height: auto;
    }
</style>

<table class="table table-responsive table-striped table-bordered" id="careers-table" width="100%">
    <thead>
     <tr>
        <th>Jobs Title</th>
        <th>Company</th>
        <th >Location</th>
        <th>Date Posted</th>
        <th>Status</th>
     </tr>
    </thead>
    <tbody>
    @foreach($careers as $career)
        <tr>
            <td>{!! $career->job_title !!}</td>
            <td>{!! $career->company !!}</td>
            <td>{!! $career->location !!}</td>
            <td>{!! $career->publish_date !!}</td>
            <td>{!! $career->status !!}</td>
            <td>
                 <!-- <a href="{{ route('admin.careers.show', collect($career)->first() ) }}">
                     <i class="material-icons text-primary leftsize" title="view jenisProdukKatalog">info</i>
                 </a> -->
                 <a href="{{ route('admin.careers.edit', collect($career)->first() ) }}">
                     <i class="material-icons text-warning leftsize" title="edit jenisProdukKatalog">edit</i>
                 </a>
                 <a href="{{ route('admin.careers.confirm-delete', collect($career)->first() ) }}" data-toggle="modal" data-target="#delete_confirm">
                     <i class="material-icons text-danger leftsize" title="delete jenisProdukKatalog">delete</i>
                 </a>
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
@section('footer_scripts')
    <div class="modal fade" id="delete_confirm" tabindex="-1" role="dialog" aria-labelledby="user_delete_confirm_title" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
            </div>
        </div>
    </div>
    <script>$(function () {$('body').on('hidden.bs.modal', '.modal', function () {$(this).removeData('bs.modal');});});</script>
<link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/buttons.bootstrap.css') }}"/>
<link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/dataTables.bootstrap.css') }}"/>
 <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/buttons.bootstrap.css') }}">
<script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/jquery.dataTables.js') }}" ></script>
 <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.bootstrap.js') }}" ></script>

    <script>
        $('#careers-table').DataTable({
                      responsive: true,
                      pageLength: 10
        });
         $('#careers-table').on( 'length.dt', function ( e, settings, len ) {
                     setTimeout(function(){
                         $('.livicon').updateLivicon();
                     },200);
         });

       </script>
@stop
