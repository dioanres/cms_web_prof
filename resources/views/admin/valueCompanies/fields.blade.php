<!-- Value Field -->
<div class="form-group col-sm-12">
    {!! Form::label('value', 'Value:') !!}
    {!! Form::text('value', null, ['class' => 'form-control']) !!}
</div>

<!-- Deskripsi Value Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('deskripsi_value', 'Deskripsi Value:') !!}
    {!! Form::textarea('deskripsi_value', null, ['class' => 'form-control', 'rows' => '5']) !!}
</div>


<!-- Submit Field -->
<div class="form-group col-sm-12 text-center">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('admin.valueCompanies.index') !!}" class="btn btn-default">Cancel</a>
</div>
