<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $valueCompany->id !!}</p>
    <hr>
</div>

<!-- Value Field -->
<div class="form-group">
    {!! Form::label('value', 'Value:') !!}
    <p>{!! $valueCompany->value !!}</p>
    <hr>
</div>

<!-- Deskripsi Value Field -->
<div class="form-group">
    {!! Form::label('deskripsi_value', 'Deskripsi Value:') !!}
    <p>{!! $valueCompany->deskripsi_value !!}</p>
    <hr>
</div>

