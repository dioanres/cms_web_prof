<table class="table table-responsive table-striped table-bordered" id="valueCompanies-table" width="100%">
    <thead>
     <tr>
        <th>Value</th>
        <th>Deskripsi Value</th>
        <th >Action</th>
     </tr>
    </thead>
    <tbody>
    @foreach($valueCompanies as $valueCompany)
        <tr>
            <td>{!! $valueCompany->value !!}</td>
            <td>{!! $valueCompany->deskripsi_value !!}</td>
            <td>
                 <a href="{{ route('admin.valueCompanies.show', collect($valueCompany)->first() ) }}">
                     <i class="material-icons text-primary leftsize" title="view valueCompany">info</i>
                 </a>
                 <a href="{{ route('admin.valueCompanies.edit', collect($valueCompany)->first() ) }}">
                     <i class="material-icons text-warning leftsize" title="edit valueCompany">edit</i>
                 </a>
                 <a href="{{ route('admin.valueCompanies.confirm-delete', collect($valueCompany)->first() ) }}" data-toggle="modal" data-target="#delete_confirm">
                     <i class="material-icons text-danger leftsize" title="delete valueCompany">delete</i>
                 </a>
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
@section('footer_scripts')
    <div class="modal fade" id="delete_confirm" tabindex="-1" role="dialog" aria-labelledby="user_delete_confirm_title" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
            </div>
        </div>
    </div>
    <script>$(function () {$('body').on('hidden.bs.modal', '.modal', function () {$(this).removeData('bs.modal');});});</script>
<link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/buttons.bootstrap.css') }}"/>
<link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/dataTables.bootstrap.css') }}"/>
 <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/buttons.bootstrap.css') }}">
<script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/jquery.dataTables.js') }}" ></script>
 <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.bootstrap.js') }}" ></script>

    <script>
        $('#valueCompanies-table').DataTable({
                      responsive: true,
                      pageLength: 10
        });
         $('#valueCompanies-table').on( 'length.dt', function ( e, settings, len ) {
                     setTimeout(function(){
                         $('.livicon').updateLivicon();
                     },200);
         });

       </script>
@stop
