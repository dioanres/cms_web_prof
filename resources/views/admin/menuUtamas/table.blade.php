<table class="table table-responsive table-striped table-bordered" id="menuUtamas-table" width="100%">
    <thead>
     <tr>
        <th>Nama Menu</th>
        <th >Action</th>
     </tr>
    </thead>
    <tbody>
    @foreach($menuUtamas as $menuUtama)
        <tr>
            <td>{!! $menuUtama->nama_menu !!}</td>
            <td>
                 <a href="{{ route('admin.menuUtamas.show', collect($menuUtama)->first() ) }}">
                     <i class="material-icons text-primary leftsize" title="view menuUtama">info</i>
                 </a>
                 <a href="{{ route('admin.menuUtamas.edit', collect($menuUtama)->first() ) }}">
                     <i class="material-icons text-warning leftsize" title="edit menuUtama">edit</i>
                 </a>
                 <!-- <a href="{{ route('admin.menuUtamas.confirm-delete', collect($menuUtama)->first() ) }}" data-toggle="modal" data-target="#delete_confirm">
                     <i class="material-icons text-danger leftsize" title="delete menuUtama">delete</i>
                 </a> -->
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
@section('footer_scripts')
    <div class="modal fade" id="delete_confirm" tabindex="-1" role="dialog" aria-labelledby="user_delete_confirm_title" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
            </div>
        </div>
    </div>
    <script>$(function () {$('body').on('hidden.bs.modal', '.modal', function () {$(this).removeData('bs.modal');});});</script>
<link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/buttons.bootstrap.css') }}"/>
<link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/dataTables.bootstrap.css') }}"/>
 <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/buttons.bootstrap.css') }}">
<script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/jquery.dataTables.js') }}" ></script>
 <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.bootstrap.js') }}" ></script>

    <script>
        $('#menuUtamas-table').DataTable({
                      responsive: true,
                      pageLength: 10
        });
         $('#menuUtamas-table').on( 'length.dt', function ( e, settings, len ) {
                     setTimeout(function(){
                         $('.livicon').updateLivicon();
                     },200);
         });

       </script>
@stop
