<!-- Nama Menu Field -->
<div class="form-group col-sm-12">
    {!! Form::label('nama_menu', 'Nama Menu:') !!}
    {!! Form::text('nama_menu', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12 text-center">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('admin.menuUtamas.index') !!}" class="btn btn-default">Cancel</a>
</div>
