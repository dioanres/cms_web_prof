@extends('admin/layouts/default')

@section('title')
MenuUtamas
@parent
@stop

@section('content')
  @include('core-templates::common.errors')
    <section class="content-header">
     <h1>MenuUtamas Edit</h1>
     <ol class="breadcrumb">
         <li>
             <a href="{{ route('admin.dashboard') }}"> <i class="material-icons text-primary leftsize">home</i>
                 Dashboard
             </a>
         </li>
         <li>MenuUtamas</li>
         <li class="active">Edit MenuUtamas </li>
     </ol>
    </section>
    <section class="content paddingleft_right15">
      <div class="row">
      <div class="panel panel-primary">
            <div class="panel-heading">
                <h4 class="panel-title"> <i class="material-icons text-primary leftsize">edit</i>
                    Edit  MenuUtama
                </h4></div>
            <br />
        <div class="panel-body">
        {!! Form::model($menuUtama, ['route' => ['admin.menuUtamas.update', collect($menuUtama)->first() ], 'method' => 'patch']) !!}

        @include('admin.menuUtamas.fields')

        {!! Form::close() !!}
        </div>
      </div>
    </div>
   </section>
 @stop
