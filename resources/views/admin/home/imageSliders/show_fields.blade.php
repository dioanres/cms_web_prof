<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $imageSlider->id !!}</p>
    <hr>
</div>

<!-- Image Field -->
<div class="form-group">
    {!! Form::label('image', 'Image:') !!}
    <p>{!! $imageSlider->image !!}</p>
    <hr>
</div>

<!-- Path Image Field -->
<div class="form-group">
    {!! Form::label('Path_image', 'Path Image:') !!}
    <p>{!! $imageSlider->Path_image !!}</p>
    <hr>
</div>

