<table class="table table-responsive table-striped table-bordered" id="imageSliders-table" width="100%">
    <thead>
     <tr>
        <th>Image</th>
        <th>Title</th>
        <th>Status</th>
        <th >Action</th>
     </tr>
    </thead>
    <tbody>
    @foreach($imageSliders as $imageSlider)
        <tr>
            <td><img src="{{URL::to('uploads/home/slider/'.$imageSlider->image)}}" class="img-upload" alt="Image"></td>
            <td>{!! $imageSlider->title !!}</td>
            <td>
                @if($imageSlider->active == 1)
                    <button class="btn btn-danger btn-sm">Non Aktifkan</button>
                @else
                <button class="btn btn-success">Aktifkan</button>
                @endif
            </td>
            <td>
                 
                 <a href="{{ route('admin.home.imageSliders.edit', ['id' => $imageSlider->id, 'type' => Request::segment(4)] ) }}">
                 <button type="button" class="btn btn-warning btn-sm">Edit</button>
                 </a>
                 <!-- <a href="{{ route('admin.home.imageSliders.confirm-delete', collect($imageSlider)->first() ) }}" data-toggle="modal" data-target="#delete_confirm">
                     <i class="material-icons text-danger leftsize" title="delete imageSlider">delete</i>
                 </a> -->
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
@section('footer_scripts')
    <div class="modal fade" id="delete_confirm" tabindex="-1" role="dialog" aria-labelledby="user_delete_confirm_title" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
            </div>
        </div>
    </div>
    <script>$(function () {$('body').on('hidden.bs.modal', '.modal', function () {$(this).removeData('bs.modal');});});</script>
<link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/buttons.bootstrap.css') }}"/>
<link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/dataTables.bootstrap.css') }}"/>
 <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/buttons.bootstrap.css') }}">
<script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/jquery.dataTables.js') }}" ></script>
 <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.bootstrap.js') }}" ></script>

    <script>
        $('#imageSliders-table').DataTable({
                      responsive: true,
                      pageLength: 10
        });
         $('#imageSliders-table').on( 'length.dt', function ( e, settings, len ) {
                     setTimeout(function(){
                         $('.livicon').updateLivicon();
                     },200);
         });

       </script>
@stop
