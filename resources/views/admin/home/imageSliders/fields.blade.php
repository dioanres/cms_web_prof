<?php
$cls = '';

if(empty($content)) {
    $cls = 'new';
} else {
    if(!empty($content->image)) {
        $cls = 'exists';
    } else {
        $cls = 'new';
    }
}

?>
<div class="form-group col-sm-12">
<span> <b>Ukuran Gambar 1920 x 877 .jpg/.jpeg</b></span>
<br/>
    <div class="fileinput fileinput-<?= $cls ?>" data-provides="fileinput">

        <div class="fileinput-new thumbnail">
        </div>

        <div class="fileinput-preview fileinput-exists thumbnail">
            @if(!empty($karyawan))
            <img src="{{URL::to('uploads/home/slider/'.$imageSlider->image)}}">
            <input type="text" hidden name="image" value="{{ $imageSlider->image }}">
            @endif
        </div>
        <div>
        
            <br/>
            <span class="btn btn-primary btn-file">
                <span class="fileinput-new">Select image</span>
                <span class="fileinput-exists">Change</span>
                <input type="file" name="image">
            </span>
            <a href="#" class="btn btn-primary fileinput-exists" data-dismiss="fileinput">Remove</a>
        </div>
    </div>
</div>
<!-- Path Image Field -->
<div class="form-group col-sm-12">
    {!! Form::label('title', 'Title Image:') !!}
    {!! Form::text('title', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group col-sm-12">
    {!! Form::label('sub_title', 'Sub Title Image:') !!}
    {!! Form::text('sub_title', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group col-sm-12">
    {!! Form::label('description', 'Description:') !!}
    {!! Form::textarea('description', null, ['class' => 'form-control']) !!}
</div>
<!-- Submit Field -->
<div class="form-group col-sm-12 text-center">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('admin.home.imageSliders.index',Request::segment(5)) !!}" class="btn btn-default">Cancel</a>
</div>

<input type="hidden" name="type" value="{{Request::segment(5)}}">