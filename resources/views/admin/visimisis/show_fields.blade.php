<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $visimisi->id !!}</p>
    <hr>
</div>

<!-- Id Pilar Usaha Field -->
<div class="form-group">
    {!! Form::label('id_pilar_usaha', 'Id Pilar Usaha:') !!}
    <p>{!! $visimisi->id_pilar_usaha !!}</p>
    <hr>
</div>

<!-- Visi Field -->
<div class="form-group">
    {!! Form::label('visi', 'Visi:') !!}
    <p>{!! $visimisi->visi !!}</p>
    <hr>
</div>

<!-- Misi Field -->
<div class="form-group">
    {!! Form::label('misi', 'Misi:') !!}
    <p>{!! $visimisi->misi !!}</p>
    <hr>
</div>

