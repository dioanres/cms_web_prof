<table class="table table-responsive table-striped table-bordered" id="visimisis-table" width="100%">
    <thead>
     <tr>
        <th>Id Pilar Usaha</th>
        <th>Visi</th>
        <th>Misi</th>
        <th >Action</th>
     </tr>
    </thead>
    <tbody>
    @foreach($visimisis as $visimisi)
        <tr>
            <td>{!! \App\Models\PilarUsaha::select('nama_pilar_usaha')->where('id',$visimisi->id_pilar_usaha)->first()->nama_pilar_usaha !!}</td>
            <td>{!! $visimisi->visi !!}</td>
            <td>{!! $visimisi->misi !!}</td>
            <td>
                 <a href="{{ route('admin.visimisis.edit', collect($visimisi)->first() ) }}">
                     <i class="material-icons text-warning leftsize" title="edit visimisi">edit</i>
                 </a>
                 <a href="{{ route('admin.visimisis.confirm-delete', collect($visimisi)->first() ) }}" data-toggle="modal" data-target="#delete_confirm">
                     <i class="material-icons text-danger leftsize" title="delete visimisi">delete</i>
                 </a>
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
@section('footer_scripts')
    <div class="modal fade" id="delete_confirm" tabindex="-1" role="dialog" aria-labelledby="user_delete_confirm_title" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
            </div>
        </div>
    </div>
    <script>$(function () {$('body').on('hidden.bs.modal', '.modal', function () {$(this).removeData('bs.modal');});});</script>
<link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/buttons.bootstrap.css') }}"/>
<link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/dataTables.bootstrap.css') }}"/>
 <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/buttons.bootstrap.css') }}">
<script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/jquery.dataTables.js') }}" ></script>
 <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.bootstrap.js') }}" ></script>

    <script>
        $('#visimisis-table').DataTable({
                      responsive: true,
                      pageLength: 10
        });
         $('#visimisis-table').on( 'length.dt', function ( e, settings, len ) {
                     setTimeout(function(){
                         $('.livicon').updateLivicon();
                     },200);
         });

       </script>
@stop
