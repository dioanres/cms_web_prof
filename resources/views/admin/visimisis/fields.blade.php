<!-- Id Pilar Usaha Field -->
<div class="form-group col-sm-12">
    {!! Form::label('id_pilar_usaha', 'Id Pilar Usaha:') !!}
    {!! Form::select('id_pilar_usaha', $pilar_usahas, null, ['class' => 'form-control']) !!}
</div>

<!-- Visi Field -->
<!-- <div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('visi', 'Visi:') !!}
    {!! Form::textarea('visi', null, ['class' => 'form-control', 'rows' => '5']) !!}
</div> -->
<div class="form-group col-sm-12">
    <div class="panel panel-primary">
        <div class="panel-heading">
            <div class="text-muted bootstrap-admin-box-title editor-clr">
                <h3 class="panel-title">
                    Visi</h3>
            </div>
        </div>
        <div class="bootstrap-admin-panel-content">
            <textarea id="ckeditor_full" class="ckeditor_full"
                name="visi">{{ $visimisi != null ? $visimisi->visi : '' }}</textarea>
        </div>
    </div>
</div>

<!-- Misi Field -->
<!-- <div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('misi', 'Misi:') !!}
    {!! Form::textarea('misi', null, ['class' => 'form-control', 'rows' => '5']) !!}
</div> -->

<div class="form-group col-sm-12">
    <div class="panel panel-primary">
        <div class="panel-heading">
            <div class="text-muted bootstrap-admin-box-title editor-clr">
                <h3 class="panel-title">
                    Misi</h3>
            </div>
        </div>
        <div class="bootstrap-admin-panel-content">
            <textarea id="ckeditor_full" class="ckeditor_full"
                name="misi">{{ $visimisi != null ? $visimisi->misi : '' }}</textarea>
        </div>
    </div>
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12 text-center">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('admin.visimisis.index') !!}" class="btn btn-default">Cancel</a>
</div>
