
<!-- <li class="{{ Request::is('admin/menuUtamas*') ? 'active' : '' }}">
    <a href="{!! route('admin.menuUtamas.index') !!}">
        <i class="material-icons text-danger leftsize">settings</i>
        Menu Utama
    </a>
</li> -->

<li class="{{ Request::is('admin/*contents*') || Request::is('admin/valueCompanies*') || Request::is('admin/alamats*') || Request::is('admin/home/imageSliders/home*') ? 'active' : '' }}">
    <a href="#">
        <i class="material-icons text-info leftsize">bookmark</i>
        <span class="title">PAB Group</span>
        <span class="fa arrow"></span>
    </a>
    <ul class="sub-menu">
        <li class="{{ Request::is('admin/home/imageSliders*') ? 'active' : '' }}">
            <a href="{!! route('admin.home.imageSliders.index',['type' => 'home']) !!}">
            <i class="material-icons">bookmark</i>
                Image Slider
            </a>
        </li>
        <li {!! (Request::is('admin/'.config('menu.about_us').'/contents') ? 'class="active"' : '' ) !!}>
            <a href="{{ URL::to('admin/'.config('menu.about_us').'/contents') }} ">
                <i class="material-icons">bookmark</i>
                About PAB Group
            </a>
        </li>
       
        <li class="{{ Request::is('admin/social_media*') ? 'active' : '' }}">
            <a href="{!! route('admin.social_media.index') !!}">
            <i class="material-icons">bookmark</i>
                Social Media
            </a>
        </li>
        <!-- <li {!! (Request::is('admin/'.config('menu.value_pab').'/contents') ? 'class="active"' : '' ) !!}>
        <a href="{{ URL::to('admin/'.config('menu.value_pab').'/contents') }} ">
                <i class="material-icons">keyboard_arrow_right</i>
                Value
            </a>
        </li> -->
        <!-- <li {!! (Request::is('admin/'.config('menu.clients').'/contents') ? 'class="active"' : '' ) !!}>
        <a href="{{ URL::to('admin/'.config('menu.clients').'/contents') }} ">
                <i class="material-icons">keyboard_arrow_right</i>
                Clients
            </a>
        </li> -->
        <!-- <li {!! (Request::is('admin/'.config('menu.csr').'/contents') ? 'class="active"' : '' ) !!}>
        <a href="{{ URL::to('admin/contents/').'/'.config('menu.csr') }} ">
                <i class="material-icons">keyboard_arrow_right</i>
                Gallery
            </a>
        </li> -->
       
        <!-- <li class="{{ Request::is('admin/alamats*') ? 'active' : '' }}">
            <a href="{!! route('admin.alamats.index') !!}">
                <i class="material-icons">keyboard_arrow_right</i>
                Alamat
            </a>
        </li> -->
    </ul>
</li>
<li class="{{ Request::is('admin/services*') ? 'active' : '' }}">
    <a href="{!! route('admin.services.index') !!}">
    <i class="material-icons text-info leftsize">bookmark</i>
               Services
    </a>
</li>
<!-- </li><li class="{{ Request::is('admin/pilarUsahas*') ? 'active' : '' }}">
    <a href="{!! route('admin.pilarUsahas.index') !!}">
    <i class="material-icons text-info leftsize">bookmark</i>
               Business Unit
    </a>
</li> -->

<li class="{{ Request::is('admin/*pilarUsahas*') || Request::is('admin/home/index/imageSliders*') ||Request::is('admin/home/imageSliders/bussinessUnit*') ? 'active' : '' }}">
    <a href="#">
        <i class="material-icons text-info leftsize">bookmark</i>
        <span class="title">Bussiness Unit</span>
        <span class="fa arrow"></span>
    </a>
    <ul class="sub-menu">
        <li class="{{ Request::is('admin/home/imageSliders/bussinessUnit*') ? 'active' : '' }}">
            <a href="{!! route('admin.home.imageSliders.bussinessUnit.index') !!}">
            <i class="material-icons">bookmark</i>
                Image Slider
            </a>
        </li>
        <li class="{{ Request::is('admin/pilarUsahas*') ? 'active' : '' }}">
            <a href="{!! route('admin.pilarUsahas.index') !!}">
                <i class="material-icons">bookmark</i>
                Bussiness Unit List
            </a>
        </li>
    </ul>
</li>

<li class="{{ Request::is('admin/*careers*') || Request::is('admin/home/imageSliders/create/career*') ||Request::is('admin/career/imageSliders*') || Request::is('admin/register_careers*') ? 'active' : '' }}">
    <a href="#">
        <i class="material-icons text-info leftsize">bookmark</i>
        <span class="title">Career</span>
        <span class="fa arrow"></span>
    </a>
    <ul class="sub-menu">
        <li class="{{ Request::is('admin/career/imageSliders*') ? 'active' : '' }}">
            <a href="{!! route('admin.career.imageSliders.index','career') !!}">
            <i class="material-icons">bookmark</i>
                Image Slider
            </a>
        </li>
        <li class="{{ Request::is('admin/careers*') ? 'active' : '' }}">
            <a href="{!! route('admin.careers.index') !!}">
            <i class="material-icons">bookmark</i>
                List Career
            </a>
        </li>

        <li class="{{ Request::is('admin/register_careers*') ? 'active' : '' }}">
            <a href="{!! route('admin.register_careers.index') !!}">
            <i class="material-icons">bookmark</i>
                    List Job Registrant
            </a>
        </li>
    </ul>
</li>
<!-- <li class="{{ Request::is('admin/alamats*') ? 'active' : '' }}">
    <a href="{!! route('admin.alamats.index') !!}">
    <i class="material-icons text-primary leftsize">flag</i>
               Alamats
    </a>
</li> -->

<li class="{{ Request::is('admin/form_online*') ? 'active' : '' }}">
    <a href="{!! route('admin.form_online.index') !!}">
    <i class="material-icons text-info leftsize">bookmark</i>
               Form Online
    </a>
</li>

<li class="{{ Request::is('admin/setting*') ? 'active' : '' }}">
    <a href="{!! route('admin.setting.index') !!}">
    <i class="material-icons text-warning leftsize">build</i>
               Setting
    </a>
</li>

<!-- <li class="{{ Request::is('admin/visimisis*') ? 'active' : '' }}">
    <a href="{!! route('admin.visimisis.index') !!}">
    <i class="material-icons text-info leftsize">language</i>
               Visi & Misi Pilar Usaha
    </a>
</li> -->

<!-- <li class="{{ Request::is('admin/jenisProdukKatalogs*') ? 'active' : '' }}">
    <a href="{!! route('admin.jenisProdukKatalogs.index') !!}">
    <i class="material-icons text-success leftsize">shopping_cart</i>
               Jenis Produk Katalog
    </a>
</li> -->
<!-- 
<li class="{{ Request::is('admin/catalogPilarUsahas*') ? 'active' : '' }}">
    <a href="{!! route('admin.catalogPilarUsahas.index') !!}">
    <i class="material-icons text-primary leftsize">image</i>
               Katalog
    </a>
</li> -->



