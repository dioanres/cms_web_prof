<?php

/**
 * Language file for delete modal
 *
 */
return array(

    'title'         => 'Hapus Data',
    'body'			=> 'Apakah Anda yakin untuk menghapus data ini ?',
    'cancel'		=> 'Batal',
    'confirm'		=> 'Hapus',

);
