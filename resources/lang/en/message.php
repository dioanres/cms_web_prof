<?php
/**
 * Language file for item error/success messages
 *
 */

return array(

    'exists'        => 'Data Sudah ada!',
    '_not_found'     => 'Data [:id] tidak ditemukan.',
    '_name_required' => 'The name field is required',

    'success' => array(
        'create' => 'Data Berhasil disimpan.',
        'update' => 'Data Berhasil disimpan.',
        'delete' => 'Data Berhasil dihapus.',
    ),

    'delete' => array(
        'create' => 'There was an issue creating the Item. Please try again.',
        'update' => 'There was an issue updating the Item. Please try again.',
        'delete' => 'There was an issue deleting the Item. Please try again.',
    ),

    'error' => array(
        'item_exists' => 'An Item already exists with that name, names must be unique for Items.',
    ),

);
